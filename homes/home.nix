{
  imports = [
    ./package/all.nix
    ./service/automatic-shutdown.nix
    ./service/autorandr.nix
    ./service/dotfiles_auto_pull.nix
    ./service/home_manager_auto_upgrade.nix
    ./service/gpg-agent.nix
    ./service/nix_garbage_collector.nix
    # ./service/firewall_at_application_level_opensnitch.nix
    # ./service/keybase.nix
    ./fonts.nix
    ./l10n.nix
    ../nix_cache/cache.nix
  ];

  manual = {
    html.enable = true;
    manpages.enable = true;
  };

  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # needed to start custom services
  # such as: automatic-shutdown-in-the-night
  systemd.user.startServices = "sd-switch";
}
