{ pkgs, ... }:

{
  services.home-manager.autoUpgrade = {
    enable = pkgs.stdenv.hostPlatform.isLinux;
    frequency = "12:30";
  };
}
