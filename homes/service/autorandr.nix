{ pkgs, ... }:

{
  programs.autorandr.enable = pkgs.stdenv.hostPlatform.isLinux;
}
