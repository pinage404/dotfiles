{ pkgs, ... }:

{
  services.opensnitch-ui.enable = pkgs.stdenv.hostPlatform.isLinux;
}
