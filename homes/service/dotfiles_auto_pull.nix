{ config, pkgs, ... }:

{
  services.dotfiles_auto_pull = {
    enable = pkgs.stdenv.hostPlatform.isLinux;
    frequency = "12:00";
    dotfiles_dir = config.home.homeDirectory + "/Project/dotfiles";
  };
}
