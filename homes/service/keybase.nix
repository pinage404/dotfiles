{ pkgs, ... }:

{
  services.keybase.enable = pkgs.stdenv.hostPlatform.isLinux;
}
