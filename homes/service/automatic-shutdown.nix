{
  pkgs,
  ...
}:

{
  services.automatic-shutdown = {
    enable = pkgs.stdenv.hostPlatform.isLinux;
    time = "01:30";
  };
}
