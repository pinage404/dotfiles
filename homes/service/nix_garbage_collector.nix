{ lib, pkgs, ... }:

{
  # https://nixos.wiki/wiki/Storage_optimization#Automation
  nix.gc = {
    automatic = true;
    frequency = "12:30";
    options = "--delete-older-than 30d";
  };

  systemd.user.services.nix-gc = lib.mkIf pkgs.stdenv.hostPlatform.isLinux {
    Service = {
      Nice = 15;
    };
  };
}
