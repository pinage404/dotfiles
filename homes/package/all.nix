let
  cliPackages = [
    ./cli/admin.nix
    ./cli/cli.nix
    ./cli/container.nix
    ./cli/dev.nix
    ./cli/misc.nix
    ./cli/multimedia.nix
    ./cli/network.nix
    ./cli/nix.nix
    ./cli/system.nix
    ./cli/useless_so_required.nix
    ./cli/xdg.nix
  ];

  guiPackages = [
    ./gui/admin.nix
    ./gui/cli.nix
    ./gui/container.nix
    ./gui/dev.nix
    ./gui/kde.nix
    ./gui/misc.nix
    ./gui/multimedia.nix
    ./gui/network.nix
    ./gui/social.nix
    ./gui/system.nix
    ./gui/useless_so_required.nix
  ];
in
{
  imports = cliPackages ++ guiPackages;
}
