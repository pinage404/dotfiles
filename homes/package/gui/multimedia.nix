{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      # video
      dragon
      gwenview
      vlc
      unfree.spotify
      mpv
      unfree.molotov
      electronplayer
      jellyfin-media-player
      popcorntime

      # audio
      qpwgraph
      audacity

      # record
      peek
      byzanz
      simplescreenrecorder

      # image
      digikam
    ]
  );
}
