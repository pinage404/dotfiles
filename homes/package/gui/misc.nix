{
  lib,
  pkgs,
  inputs,
  system,
  ...
}:

{
  nixpkgs.config.firefox.speechSynthesisSupport = true;

  home.packages =
    (with pkgs; [
      flameshot
    ])
    ++ (lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        firefox
        brave
        chromium
        eolie
        epiphany
        falkon
        midori
        tor-browser-bundle-bin
        librewolf
        unfree.microsoft-edge
        inputs.zen-browser.packages."${system}".default

        thunderbird

        # speech synthesis
        speechd
        kdePackages.kmouth
        gspeech

        livecaptions

        # Fallback for some GTK apps
        hicolor-icon-theme
        adwaita-icon-theme

        cozy-drive

        virtualbox
        deluge

        bitwarden
        # keybase-gui
        kbfs

        organicmaps

        # libreoffice
        # paperwork
        pdfsam-basic # tool that does magic with PDF (e.g. merge)

        translatelocally
      ]
    ))
    ++ (lib.optionals pkgs.stdenv.hostPlatform.isDarwin (
      with pkgs;
      [
        brewCasks.marta
        brewCasks.zen-browser
      ]
    ));
}
