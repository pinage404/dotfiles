{
  pkgs,
  ...
}:

{
  home.packages = with pkgs; [
    lutris
    heroic
    gamescope
    wine

    kdePackages.kgeography
    kdePackages.ksquares
    kdePackages.picmi
    kdePackages.kwordquiz
  ];
}
