{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      # display fire
      aalib

      # cute characters (cat) that follow cursor
      oneko
    ]
  );
}
