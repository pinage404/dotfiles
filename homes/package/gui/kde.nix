{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      kdePackages.ki18n
      gsettings-desktop-schemas # needed to set the GTK's settings

      kcalc

      khelpcenter
      ksystemlog

      kate
      filelight

      krunner-translator
      krunner-symbols
      vscode-runner
      internal.krunner-spotify

      orca
      kdePackages.kmousetool

      kdePackages.alpaka

      kdePackages.kleopatra
      kgpg

      # VNC
      krdc
      krfb

      # Theme
      papirus-icon-theme
    ]
  );
}
