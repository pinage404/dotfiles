{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      # Git / Diff
      meld
      unfree.gitkraken
      gource

      # Image manipulation
      graphviz # dot -> image
      plantuml # plantuml -> image

      codevis # generate image with all the code
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        # Color chooser
        kcolorchooser
        gcolor3

        # Git / Diff
        kdiff3
        kompare
        gitg

        # Testing API
        insomnia

        # Testing security
        # zap

        # Image manipulation
        inkscape # SVG -> PNG

        heaptrack
        elf-dissector
      ]
    );

  programs.vscode = {
    enable = true;
    package = if pkgs.stdenv.hostPlatform.isLinux then pkgs.unfree.vscode-fhs else pkgs.unfree.vscode;
    extensions = with pkgs.unfree.vscode-extensions; [
    ];
  };
}
