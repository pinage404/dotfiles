{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      # Network admin test tools
      wireshark
    ]
  );
}
