{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      unfree.slack
      unfree.zoom-us
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        signal-desktop
        unfree.discord
      ]
    );
}
