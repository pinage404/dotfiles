{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      podman-desktop

      # Docker
      x11docker
      nx-libs # for x11docker
      tini # for x11docke
      xpra # for x11docker
    ]
  );
}
