{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      # Disk usage
      filelight
      baobab
      gdmap
      k4dirstat
      gnome-disk-utility

      # Hardware tools
      xorg.xhost

      nix-software-center
    ]
  );
}
