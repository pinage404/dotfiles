{
  pkgs,
  lib,
  ...
}:

{
  home.packages =
    (with pkgs; [
      exercism

      # tools to build things
      binutils
      patchelf
      nix-bundle
      nixbang

      # Rust
      rustup # VSCode seems to need rustup installed globally
      cargo-cache # manage Rust's cache using `cargo cache --remove-dir all --remove-if-older-than $(date --date "-30days")`

      # Git / Diff
      diffutils
      wdiff
      gitAndTools.gitFull
      gitAndTools.git-extras
      git-sizer
      git-quick-stats
      git-secret
      gitleaks
      gitAndTools.gitflow
      gitAndTools.git-absorb
      gitAndTools.delta
      gitAndTools.difftastic
      gitAndTools.git-interactive-rebase-tool
      git-trim
      git-filter-repo
      gitstats
      git-sizer
      git-gamble
      internal.tcr
      mob
      act # test GitHub Actions locally
      lazygit
      internal.git-split
      internal.git-foreach
      ghorg # clone all repo from an organization
      unfree.gk-cli
      git-igitt

      glab
      gitlab-runner
      act

      # Image manipulation
      imagemagick # image -> image
      potrace # PNG -> SVG
      autotrace # PNG -> SVG

      # Testing API
      unfree.ngrok

      # Stats
      onefetch # neofetch for a git repository : lines of code, repo, etc
      tokei # shows the number of lines of code in a folder for each language
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        git-graph
      ]
    );
}
