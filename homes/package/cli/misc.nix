{ lib, pkgs, ... }:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      # Needed to run AppImage
      appimage-run
      firejail # sandbox AppImage

      flatpak

      # speech to text
      openai-whisper
      stt

      # text to speech
      piper-tts
      mimic
      svox
      speech-tools
      # tts # broken
    ]
  );
}
