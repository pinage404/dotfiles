{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      # text to ascii
      banner
      figlet
      toilet

      # display text
      cowsay
      ponysay
      tewisay

      # generate text
      fortune

      # display a train
      sl

      # display Matrix wall
      cmatrix
      gomatrix
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        # display Matrix wall
        tmatrix
        # display text

        xcowsay
      ]
    );
}
