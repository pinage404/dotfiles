{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      agenix

      # Disk usage
      du-dust # modern Disk Usage
      duf
      agedu
      diskonaut

      # Top
      htop
      btop
      iftop
      bottom # `btm` : ksysguard in console
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        # Disk usage
        ncdu

        # Top
        sysstat
        iotop
        powertop
        nvtopPackages.amd

        # Hardware tools
        usbutils
        pmutils
        hddtemp
        lshw
        pciutils
        lm_sensors
        lsb-release
        upower
      ]
    )
    ++ lib.optionals pkgs.stdenv.hostPlatform.isx86_64 (
      with pkgs;
      [
        woeusb # burn Windows ISO image
      ]
    );
}
