{
  lib,
  pkgs,
  inputs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      nvd
      nox
      nix-bash-completions
      nix-diff
      vulnix

      manix # tool that search and display options of NixOS / Home Manager documentation
      nixos-option # tool that list current evaluated options values

      comma # run program without installing it

      # tools to explore nix-store
      nix-top
      nix-tree # interactively browse dependency graphs of Nix derivations

      # tools that help writing Nix
      nixfmt-rfc-style
      nix-init # Generate Nix packages from URLs
      nix-update
      nixpkgs-review

      snowfallorg.frost
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        internal.pushnix

        inputs.nixos-conf-editor.packages."${pkgs.system}".nixos-conf-editor

        # Tool to run binary w/o unknown path issues
        steam-run-free

        # tools to explore nix-store
        nix-du # nix-shell --packages nix-du graphviz --command 'nix-du -s=248MB | tred | dot -Tsvg > store.svg'

        # tools that help writing Nix
        nix-doc # tool to have documentation about nix function `nix-doc search mkIf ~/.nix-defexpr/channels/nixos/`
      ]
    );

  programs.nix-index.enable = true;

  programs.home-manager.enable = true;
}
