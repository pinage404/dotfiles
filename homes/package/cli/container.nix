{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      # Docker
      docker
      docker-compose

      podman

      shfmt
      dive

      oxker
      ctop # container top
      lazydocker
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        podman-compose
      ]
    );
}
