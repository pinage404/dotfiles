{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      iftop
      mtr
      dogdns
      curlie

      # Network admin test tools
      nmap

      # Network speed tools
      gping
      bandwhich # list network inputs / outputs (need to be run as root)
      speedtest-cli
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        httpie

        nettools
        nethogs # htop for network (need to be run as root)

        # Network admin test tools
        aircrack-ng

        # Network speed tools
        fast-cli
      ]
    );
}
