{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      # File system
      file
      tree
      which
      lsof
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        samba4Full
      ]
    );
}
