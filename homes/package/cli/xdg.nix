{
  lib,
  pkgs,
  ...
}:

{
  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      xdg-utils
      xdg-user-dirs
      desktop-file-utils
    ]
  );

  xdg =
    if pkgs.stdenv.hostPlatform.isLinux then
      {
        enable = true;

        userDirs = {
          enable = true;
          createDirectories = true;

          desktop = "$HOME/Desktop";
          documents = "$HOME/Document";
          download = "$HOME/Download";
          music = "$HOME/Music";
          pictures = "$HOME/Image";
          publicShare = "$HOME/Public";
          templates = "$HOME/Templates";
          videos = "$HOME/Video";

          extraConfig = {
            XDG_PROJECTS_DIR = "$HOME/Project";
          };
        };

        portal.xdgOpenUsePortal = true;
      }
    else
      { };
}
