{
  lib,
  pkgs,
  ...
}:

{
  home.packages =
    (with pkgs; [
      # Shell
      fish
      bashInteractive

      # Download
      wget
      curl
      rsync
      yt-dlp

      # Archive
      pigz
      brotli

      # Auto completion
      bash-completion
      nix-bash-completions

      # Security
      gnupg

      # Search
      ripgrep
      ripgrep-all
      sd # replace lines in files or lines in stdin without the "annoying" syntax of sed or awk

      # Fuzzy Finder
      fzf
      skim

      zoxide # smarter CD

      # Basic command replacement with modern tools
      bat # cat
      lsd # ls
      eza # ls
      fd # find but newer / faster / better

      # Better prompt
      starship

      # Text Web Browser
      browsh # try to emulate CSS ?

      # clipboard tools
      xclip
      xsel

      # Other
      htop
      jq # JSON Query
      jnv # jq but interactive
      yq # YAML Query
      xxd # show files content in hexadecimal
      hexyl # show file content in hexadecimal newer / better
      binocle # show binary file as image
      unzip
      gnused # needed by some oh-my-fish plugins
      textql # SQL's like for CSV
      tmate # simply share terminal through SSH
      mdcat # cat for markdown
      kondo # Save disk space by cleaning non-essential files from software projects
      watchexec # Execute commands when watched files change
      mprocs # Execute several commands in one command
      bench # Benchmark command time execution (better than `time`)
      runiq # Like `uniq` but can be unsorted
      broot # file explorer with fuzzy finder and many features
      neofetch # show system information
      nerdfetch # show system information using NerdFonts
      cpufetch # neofetch for CPU
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        # Text Web Browser
        elinks # simple pure text
        links2
        w3m

        # Other
        psmisc # pstree
      ]
    );

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  programs.fish.enable = true;
}
