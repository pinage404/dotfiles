{
  imports = [
    ../../package/all.nix
    ../../fonts.nix
    ../../l10n.nix
    ../../../nix_cache/cache.nix
  ];
  home.stateVersion = "23.05";
}
