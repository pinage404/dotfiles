{
  lib,
  pkgs,
  ...
}:

{
  # Find fonts installed in packages
  fonts.fontconfig.enable = true;

  home.packages = lib.optionals pkgs.stdenv.hostPlatform.isLinux (
    with pkgs;
    [
      (nerdfonts.override {
        fonts = [
          "FiraCode"
          "RobotoMono"
          "SourceCodePro"
        ];
      })
      oxygenfonts
      source-sans-pro
    ]
  );
}
