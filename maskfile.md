# Commands

- [install](#install)
  - [install detect-and-generate-hardware-configuration](#install-detect-and-generate-hardware-configuration)
- [build-boot-switch](#build-boot-switch)
- [gamble-update](#gamble-update)
- [link-dotfiles](#link-dotfiles)
- [fish](#fish)
  - [fish install-then-update](#fish-install-then-update)
  - [fish update](#fish-update)
- [flake-update](#flake-update)
- [home-manager](#home-manager)
  - [home-manager build](#home-manager-build)
  - [home-manager switch](#home-manager-switch)
- [nixos](#nixos)
  - [nixos build](#nixos-build)
  - [nixos boot](#nixos-boot)
  - [nixos switch](#nixos-switch)
- [darwin](#darwin)
  - [darwin build](#darwin-build)
  - [darwin switch](#darwin-switch)
- [brew](#brew)
  - [brew update](#brew-update)
  - [brew switch](#brew-switch)
- [nix-on-droid](#nix-on-droid)
  - [nix-on-droid build](#nix-on-droid-build)
  - [nix-on-droid switch](#nix-on-droid-switch)
- [deploy (MACHINE)](#deploy-machine)
- [lint](#lint)
  - [lint fix](#lint-fix)
- [format](#format)
- [with-nix (DEVSHELL)](#with-nix-devshell)
- [low-priority](#low-priority)
- [ring-the-bell](#ring-the-bell)
- [nix-why-is-this-installed (PACKAGE)](#nix-why-is-this-installed-package)

## install

Once you had [installed NixOS](https://nixos.org/manual/nixos/stable/#sec-installation) (or [Nix](https://nixos.org/download#download-nix) on non-NixOS distributions)

```bash
set -o errexit -o nounset -o pipefail -o errtrace

export NIX_CONFIG="extra-experimental-features = flakes nix-command"

nix shell 'nixpkgs#gitMinimal' # others commands (like `nixos-rebuild`) will need git

git clone https://gitlab.com/pinage404/dotfiles.git

cd dotfiles

nix develop ".#direnv" --command \
    direnv allow # other commands assume that direnv is allowed
```

### install detect-and-generate-hardware-configuration

On NixOS, you can generate a basic configuration

OPTIONS

- machine
  - flags: --machine
  - type: string
  - desc: Set configuration for this machine
  - required

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nixos-generate-config --dir /tmp
mkdir --parents "systems/machine/$machine/"
cp /tmp/{hardware-,}configuration.nix "systems/machine/$machine/"
```

## build-boot-switch

```bash
set -o errexit -o nounset -o pipefail -o errtrace

if [[ "$(uname -v)" =~ "NixOS" ]]; then
    $MASK nixos build
elif [ "$(uname)" == "Darwin" ]; then
    $MASK darwin build
fi
$MASK home-manager build

if [[ "$(uname -v)" =~ "NixOS" ]]; then
    $MASK nixos boot
fi

if [[ "$(uname -v)" =~ "NixOS" ]]; then
    $MASK nixos switch
elif [ "$(uname)" == "Darwin" ]; then
    $MASK darwin switch
fi
$MASK home-manager switch
```

## gamble-update

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix git-gamble):$PATH"

$MASK link-dotfiles || echo "link dotfiles failed"

$MASK topology

$MASK format
$MASK lint fix
$MASK fish update
$MASK flake-update

if [ "$(uname)" == "Darwin" ]; then
    $MASK brew update
fi

git gamble --pass --message "update Nix Flakes' dependencies"
```

## link-dotfiles

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix link-dotfiles):$PATH"

./manual_install/link-dotfiles.sh --quiet
```

## test

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake show
$MASK format
$MASK lint fix
$MASK topology
$MASK home-manager build
if [[ "$(uname -v)" =~ "NixOS" ]]; then
    $MASK nixos build-all
elif [ "$(uname)" == "Darwin" ]; then
    $MASK darwin build
fi
```

## fish

### fish install-then-update

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix fish):$PATH"

./manual_install/fish/install.fish
```

### fish update

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix fish):$PATH"

./manual_install/fish/update.fish
```

## flake-update

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority

nix flake update
```

## home-manager

### home-manager build

OPTIONS

- USER
  - flags: --user
  - type: string
  - desc: Set configuration for this user
- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix home-manager):$PATH"

home-manager build --flake ".#${USER}@${HOSTNAME}"

./dotfiles/local/bin/home-manager-diff result
rm result
```

### home-manager switch

OPTIONS

- USER
  - flags: --user
  - type: string
  - desc: Set configuration for this user
- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix home-manager):$PATH"

home-manager switch --flake ".#${USER}@${HOSTNAME}"

./dotfiles/local/bin/home-manager-diff
```

## nixos

### nixos build

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix nixos):$PATH"

nixos-rebuild build --flake ".#${HOSTNAME}"

./dotfiles/local/bin/nixos-diff result
rm result
```

### nixos list

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix flake show --json | jq ".nixosConfigurations" | jq --raw-output "keys[]"
```

### nixos build-all

```bash
set -o errexit -o nounset -o pipefail -o errtrace

for HOSTNAME in $($MASK nixos list); do
  $MASK nixos build --hostname "$HOSTNAME"
done
```

### nixos boot

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix nixos):$PATH"

sudo --bell nixos-rebuild boot --flake ".#${HOSTNAME}"

./dotfiles/local/bin/nixos-diff
```

### nixos test

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix nixos):$PATH"

sudo --bell nixos-rebuild test --flake ".#${HOSTNAME}"

./dotfiles/local/bin/nixos-diff
```

### nixos switch

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix nixos):$PATH"

sudo --bell nixos-rebuild switch --flake ".#${HOSTNAME}"

./dotfiles/local/bin/nixos-diff
```

## darwin

### darwin build

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix darwin):$PATH"

darwin-rebuild build --flake ".#${HOSTNAME}"
```

### darwin switch

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix darwin):$PATH"

$MASK ring-the-bell

darwin-rebuild switch --flake ".#${HOSTNAME}"
```

## brew

### brew update

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority

brew tap homebrew/bundle
brew update
brew bundle --file "./manual_install/mac_os_x_homebrew/Brewfile"
```

### brew switch

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority

export HOMEBREW_NO_AUTO_UPDATE=1
brew tap homebrew/bundle
brew bundle --no-upgrade --cleanup --file "./manual_install/mac_os_x_homebrew/Brewfile"
```

## nix-on-droid

Currently, [hostname can't be defined with nix-on-droid](https://github.com/nix-community/nix-on-droid/issues/51)

### nix-on-droid build

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix nix-on-droid):$PATH"

nix-on-droid build --flake ".#${HOSTNAME}"
```

### nix-on-droid switch

OPTIONS

- HOSTNAME
  - flags: --hostname
  - type: string
  - desc: Set configuration for this hostname

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority
# shellcheck disable=SC2155
export PATH="$($MASK with-nix nix-on-droid):$PATH"

nix-on-droid switch --flake ".#${HOSTNAME}"
```

## deploy (MACHINE)

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority

git status --short

nix run ".#deploy-$MACHINE"
```

## topology

```bash
set -o errexit -o nounset -o pipefail -o errtrace

nix build .#topology
cp ./result/*.svg ./docs/
chmod u+w ./docs/main.svg ./docs/network.svg
```

## lint

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix lint):$PATH"

set -o xtrace
masklint run
# shellcheck disable=SC2046
shellcheck $(fd --exclude 'dotbot' '\.(ba)?sh$')
deadnix --no-underscore --fail
statix check
nixpkgs-lint --include-unfinished-lints .
```

### lint fix

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix lint):$PATH"

deadnix --no-underscore --edit --fail
statix fix
```

## format

```bash
set -o errexit -o nounset -o pipefail -o errtrace
# shellcheck disable=SC2155
export PATH="$($MASK with-nix format):$PATH"

# shellcheck disable=SC2046
nix fmt $(fd --exclude 'dotbot' '\.nix$')
# shellcheck disable=SC2046
shellcheck --format=diff $(fd --exclude 'dotbot' '\.(ba)?sh$') | git apply --allow-empty
```

## with-nix (DEVSHELL)

```bash
set -o errexit -o nounset -o pipefail -o errtrace
$MASK low-priority

# shellcheck disable=SC2016
nix develop ".#${DEVSHELL}" --command sh -c 'echo $PATH'
```

## low-priority

```bash
set -o errexit -o nounset -o pipefail -o errtrace

# current process is this script
# parent process is mask
# parent of parent process should be the script that invoked this task
PID=$(ps -p $PPID -o ppid=)

renice -n 19 -p "$PID" >/dev/null
if [ "$(uname)" == "Linux" ]; then
    ionice -c 3 -p "$PID"
fi
```

## ring-the-bell

```bash
echo -ne '\007'
```

## nix-why-is-this-installed (PACKAGE)

```bash
DERIVATION_PATHS="/nix/store/*$PACKAGE*"

for DERIVATION_PATH in $DERIVATION_PATHS; do
    echo "$DERIVATION_PATH"
    nix why-depends ".#nixosConfigurations.${HOSTNAME}.config.system.build.toplevel" "$DERIVATION_PATH"
    nix why-depends ".#homeConfigurations.${USER}@${HOSTNAME}.activationPackage" "$DERIVATION_PATH"
    echo ""
done
```
