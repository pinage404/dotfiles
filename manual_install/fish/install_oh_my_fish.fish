#!/usr/bin/env fish

# install oh my fish
curl -sSLo /tmp/install_oh_my_fish https://get.oh-my.fish
fish /tmp/install_oh_my_fish --noninteractive
fish --command="omf install"
fish --command="omf update"
