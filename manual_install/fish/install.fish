#!/usr/bin/env fish

set DIR (cd (dirname (status -f)); and pwd)


# install oh my fish
source $DIR/install_oh_my_fish.fish

curl -sSL https://raw.githubusercontent.com/jorgebucaran/fisher/4.4.5/functions/fisher.fish | source

source $DIR/update.fish
