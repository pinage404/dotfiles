#!/usr/bin/env bash

set -e

BASEDIR="$(realpath "$(dirname "$(dirname "${BASH_SOURCE[0]}")")")"

CONFIG="${BASEDIR}/manual_install/install.conf.yaml"
DOTBOT_DIR="dotbot"

DOTBOT_BIN="bin/dotbot"

cd "${BASEDIR}"
git -C "${DOTBOT_DIR}" submodule sync --quiet --recursive
git submodule update --init --recursive "${DOTBOT_DIR}"

"${BASEDIR}/${DOTBOT_DIR}/${DOTBOT_BIN}" --base-directory "${BASEDIR}" --config-file "${CONFIG}" "${@}"
