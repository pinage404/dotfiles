#!/usr/bin/env sh
set -o errexit -o nounset

# shellcheck disable=SC1091
. "$(dirname "$(dirname "$0")")/util.sh"

download_and_install_appimage_list "$(dirname "$0")/appimage_gui_url_list.yaml"

install_appimage_with_nixos_list "$(dirname "$0")/appimage_gui_url_list.yaml"
