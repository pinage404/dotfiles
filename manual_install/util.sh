#!/usr/bin/env bash

exec_foreach_list() {
	set -e
	grep -v "^\s*$" "$2" | grep -v "^#.*$" | while read -r line; do
		$1 "$line"
	done
	set +e
}

download_and_install_appimage() {
	name=$1
	url=$2
	# shellcheck disable=SC2001
	file=$(echo "$name" | sed "s/[^a-zA-Z0-9._-]/_/")
	path="$(realpath ~)/.local/bin/${file}.AppImage"

	echo "Downloading then installing $name"

	mkdir -p "$(dirname "$path")"
	wget "$url" -qO "$path"
	chmod +x "$path"
}

_download_and_install_appimage() {
	name=${1%: *}
	url=${1##*: }
	download_and_install_appimage "$name" "$url"
}

download_and_install_appimage_list() {
	exec_foreach_list _download_and_install_appimage "$1"
}

install_appimage_with_nixos() {
	name=$1
	url=$2
	# shellcheck disable=SC2001
	file=$(echo "$name" | sed "s/[^a-zA-Z0-9._-]/_/")
	path="$(realpath ~)/.local/bin/${file}.AppImage"
	bin_path="$(realpath ~)/.local/bin/${file}"
	app_path="$(realpath ~)/.local/share/applications/nixos-${file}.desktop"

	echo "Installing $name"

	cat >"$bin_path" <<EOF
#!/bin/sh
appimage-run $path \$@
EOF
	chmod +x "$bin_path"

	mkdir -p "$(dirname "$app_path")"
	cat >"$app_path" <<EOF
[Desktop Entry]
Type=Application
Name=Nix $name
GenericName=Nix $name
Exec=$bin_path
EOF
	chmod +x "$app_path"
}

_install_appimage_with_nixos() {
	name=${1%: *}
	url=${1##*: }
	install_appimage_with_nixos "$name" "$url"
}

install_appimage_with_nixos_list() {
	exec_foreach_list _install_appimage_with_nixos "$1"
}
