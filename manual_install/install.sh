#!/usr/bin/env sh
set -o errexit -o nounset

# shellcheck disable=SC1091
cd "$(dirname "$(dirname "$(realpath "$0")")")" || exit

nix-shell --packages python --command "./manual_install/link-dotfiles.sh --quiet"

./manual_install/gui/install_from_appimage_package.bash

./manual_install/fish/install.fish
