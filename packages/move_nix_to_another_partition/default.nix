{
  writeShellApplication,
  ...
}:

writeShellApplication {
  name = "move_nix_to_another_partition";
  text = builtins.readFile ./move_nix_to_another_partition.sh;
}
