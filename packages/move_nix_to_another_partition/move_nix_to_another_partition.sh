#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail -o errtrace -o xtrace

export NIX_CONFIG='extra-experimental-features = flakes nix-command'

PARTITION="${1:-/dev/disk/by-label/nix}"

mount_new_partition() {
    mount "$PARTITION" /mnt
}

copy_everything_from_the_current_nix_folder() {
    # Trailing slash are important
    rsync --archive --acls --one-file-system --verbose /nix/store/ /mnt/store
    rsync --archive --acls --one-file-system --verbose /nix/var/ /mnt/var
}

use_the_new_partition_as_new_nix() {
    umount /mnt
    mount "$PARTITION" /nix
}

restart_nix_daemon() {
    # Restarting in 3 steps works better than restarting both at once
    systemctl stop nix-daemon.service
    systemctl restart nix-daemon.socket
    systemctl start nix-daemon.service
}

main() {
    mount_new_partition
    copy_everything_from_the_current_nix_folder
    use_the_new_partition_as_new_nix
    restart_nix_daemon
}

main
