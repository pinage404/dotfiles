{
  lib,
  writeText,
  inputs,
  python3Packages,
  makeWrapper,
  dbus,
  kate,
  kdePackages,
  ...
}:

let
  version = "0.0"; # this is a fake version, the version is controlled by flake

  setupPy = writeText "setup.py" ''
    from setuptools import setup
    setup(
      name='krunner-spotify',
      version='${version}',
      scripts=[
        'src/KRunnerSpotify.py',
      ],
    )
  '';
in
python3Packages.buildPythonApplication rec {
  pname = "krunner-spotify";
  inherit version;

  src = inputs.krunner-spotify;

  nativeBuildInputs = [
    makeWrapper
    dbus
    kate
  ];

  propagatedBuildInputs = [
    python3Packages.setuptools
    python3Packages.dbus-python
    python3Packages.pygobject3
    python3Packages.spotipy
  ];

  postPatch = ''
    cp ${setupPy} ${setupPy.name}
  '';

  postInstall = ''
    MAIN_PROGRAM="$out/bin/KRunnerSpotify"

    # i don't know how to package something with Nix that want to write in the home folder
    # hardcode config in the package instead of the home folder
    CONFIG_PATH="$out/config/KRunner-Spotify/KRunner-Spotify.config"

    substituteInPlace $out/${python3Packages.python.sitePackages}/Config.py \
      --replace-fail '~") + "/.config/KRunner-Spotify/KRunner-Spotify.config"' "$CONFIG_PATH\")"

    install -D \
      ./KRunner-Spotify.config \
      $CONFIG_PATH

    install -D \
      ./plasma-runner-KRunnerSpotify.desktop \
      $out/share/krunner/dbusplugins/plasma-runner-KRunnerSpotify.desktop

    install -D \
      ./org.kde.KRunnerSpotify.service \
      $out/share/dbus-1/services/org.kde.KRunnerSpotify.service

    install -D \
      ./icons/* \
      --target-directory=$out/share/pixmaps

    makeWrapper ${python3Packages.python.interpreter} $MAIN_PROGRAM \
      --prefix PYTHONPATH : ${
        python3Packages.makePythonPath (propagatedBuildInputs ++ [ (placeholder "out") ])
      } \
      --add-flags $out/${python3Packages.python.sitePackages}/KRunnerSpotify.py
  '';

  postFixup = ''
    substituteInPlace $out/share/dbus-1/services/org.kde.KRunnerSpotify.service \
      --replace-fail '/usr/bin/python3 "%{PROJECTDIR}/KRunnerSpotify.py"' "$MAIN_PROGRAM"

    substituteInPlace $CONFIG_PATH \
      --replace-fail "/usr/bin/kate" "${lib.makeBinPath [ kate ]}/kate"
  '';

  meta = {
    description = "Allows you to easily control Spotify via KRunner";
    homepage = "https://github.com/MartijnVogelaar/krunner-spotify";
    license = lib.licenses.gpl3Only;
    mainProgram = "KRunnerSpotify";
    inherit (kdePackages.krunner.meta) platforms;
  };
}
