{
  pkgs,
  inputs,
  ...
}:

let
  topology = import inputs.nix-topology {
    inherit pkgs;
    modules = [
      (
        { config, ... }:
        {
          inherit (pkgs.lib.self) nixosConfigurations;

          networks.home.name = "Home";

          nodes.router = config.lib.topology.mkRouter "Box" {
            interfaceGroups = [
              [
                "eth1"
                "eth2"
                "eth3"
                "eth4"
              ]
              [ "wifi" ]
              [ "wan1" ]
            ];
            connections.eth1 = config.lib.topology.mkConnectionRev "dell-optiplex-3050" "eth0";
            connections.eth2 = config.lib.topology.mkConnectionRev "raspberry-pi-3b-black" "eth0";
            connections.eth3 = config.lib.topology.mkConnectionRev "raspberry-pi-3b-white" "eth0";
            connections.wifi = [
              (config.lib.topology.mkConnection "framework-16" "wifi")
              (config.lib.topology.mkConnection "gigabyte-sabre-15" "wifi")
            ];
          };

          nodes.internet = config.lib.topology.mkInternet {
            connections = config.lib.topology.mkConnectionRev "router" "wan1";
          };
        }
      )
    ];
  };
in
topology.config.output
