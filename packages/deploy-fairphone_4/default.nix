{
  pkgs,
  lib,
  ...
}:

# inspired by
# https://github.com/MatthewCroughan/nixinate/issues/34#issue-1425896087
let
  user = "nix-on-droid";
  host = "fairphone_4.local";
  machine = "fairphone_4";
  ssh-target = "ssh://${user}@${host}";
in
pkgs.writeShellApplication {
  name = "deploy-to-fairphone";
  text = ''
    set -o errexit -o nounset -o pipefail -o errtrace -o xtrace

    # build locally
    nix build .#nixOnDroidConfigurations.${machine}.activationPackage --impure

    # the following command should work but fails with the following error
    # nix-store: command not found
    # nix copy ${pkgs.lib.self} --to ${ssh-target}

    SSH_SOURCE_USER="$(${lib.getExe' pkgs.toybox "whoami"})"
    SSH_SOURCE_HOST="$(${lib.getExe' pkgs.iproute2 "ip"} addr show up | ${lib.getExe pkgs.ripgrep} --only-matching 192.168.1.\\d+ | ${lib.getExe' pkgs.toybox "head"} --lines 1)"
    SSH_SOURCE="ssh-ng://$SSH_SOURCE_USER@$SSH_SOURCE_HOST"

    ssh -t ${ssh-target} "
      set -o errexit -o nounset -o pipefail -o xtrace

      # fix SSH on nix-on-droid
      export PATH=\$PATH:/data/data/com.termux.nix/files/home/.nix-profile/bin

      # copy the from source to nix-on-droid
      nix copy ${pkgs.lib.self} --from $SSH_SOURCE

      # rebuild
      nix-on-droid switch --flake '${pkgs.lib.self}#${machine}'
    "
  '';
}
