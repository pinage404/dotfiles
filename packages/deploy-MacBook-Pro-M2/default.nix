{
  pkgs,
  ...
}:

# inspired by
# https://github.com/MatthewCroughan/nixinate/issues/34#issue-1425896087
let
  user = "eb";
  host = "MacBook-Pro-M2.local";
  machine = "MacBook-Pro-M2";
in
pkgs.writeShellApplication {
  name = "deploy-to-mac";
  text = ''
    set -o errexit -o nounset -o pipefail -o errtrace -o xtrace
    nix copy ${pkgs.lib.self} --to ssh://${user}@${host}
    ssh -t ${user}@${host} "darwin-rebuild switch --flake ${pkgs.lib.self}#${machine}"
  '';
}
