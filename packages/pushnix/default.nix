# copied from https://github.com/arnarg/config/commit/76322ba1a088991d60845300246566ba9ccaa43b
{
  lib,
  inputs,
  buildGoModule,
  makeWrapper,
  gitMinimal,
  openssh,
  ...
}:

buildGoModule rec {
  pname = "pushnix";
  version = "fixed-by-flake";

  src = inputs.pushnix;

  vendorHash = "sha256-kLeRhHdP+FdZUuYoKCAsIFLOWBCBpfmGGvHHB7nGV9Q=";

  subPackages = [ "." ];

  nativeBuildInputs = [ makeWrapper ];

  ldflags = [ "-X main.Version=${version}" ];

  postFixup = ''
    wrapProgram $out/bin/pushnix \
      --prefix PATH : "${
        lib.makeBinPath [
          gitMinimal
          openssh
        ]
      }"
  '';

  meta = {
    description = "Simple cli utility that pushes NixOS configuration and triggers a rebuild using ssh.";
    homepage = "https://github.com/arnarg/pushnix";
    license = lib.licenses.mit;
    platforms = lib.platforms.linux;
  };
}
