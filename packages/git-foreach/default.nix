{
  lib,
  inputs,
  stdenv,
  makeWrapper,
  getopt,
  findutils,
  gnused,
  ...
}:

stdenv.mkDerivation {
  pname = "git-foreach";
  version = "fixed-by-flake";

  src = inputs.git-foreach;

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir --parents $out/bin
    cp git-foreach $out/bin
  '';

  postFixup = ''
    substituteInPlace $out/bin/git-foreach \
      --replace-fail "/usr/bin/getopt" "${lib.getExe getopt}"

    wrapProgram $out/bin/git-foreach \
      --prefix PATH : "${
        lib.makeBinPath [
          findutils
          gnused
        ]
      }"
  '';

  meta = {
    description = "Perform a git command on multiple repositories";
    homepage = "https://github.com/d-e-s-o/git-foreach";
    license = lib.licenses.gpl3Only;
    mainProgram = "git-foreach";
    platforms = lib.platforms.all;
  };
}
