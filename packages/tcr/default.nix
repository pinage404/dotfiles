{
  lib,
  inputs,
  buildGo122Module,
}:

buildGo122Module {
  pname = "tcr";
  version = "fixed-by-flake";

  src = inputs.tcr;

  vendorHash = "sha256-+p4EBjXmQEwPTNks7fnP6h3NikMwyMGcVondmbN/NXc=";

  env.GOWORK = "off";
  modRoot = "./src";

  doCheck = false; # i don't understand why the build fails when the tests are enabled

  meta = {
    description = "TCR (Test && Commit || Revert) utility";
    homepage = "https://github.com/murex/TCR";
    license = lib.licenses.mit;
    mainProgram = "tcr";
    platforms = lib.platforms.all;
  };
}
