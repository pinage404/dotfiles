# pinage404's dotfiles and configurations

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

- [Commands](#commands)
- [Install NixOS on Raspberry PI](#install-nixos-on-raspberry-pi)
- [Manage repository from a remote machine](#manage-repository-from-a-remote-machine)
- [Arborescence](#arborescence)
- [Packages](#packages)
- [Story](#story)
- [Overview](#overview)
- [Licence](#licence)

This is the cumulative **efforts of several years** of _trying_ to craft _my_ **perfect setup** :

- configurations
- programs installed
- services run
- scripts
- tools for **reinstalling everything in just a few minutes**
- etc

## Commands

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

## Install NixOS on Raspberry PI

To [Install NixOS on Raspberry PI follow this](./docs/install_nixos_on_raspberry_pi.md)

## Manage repository from a remote machine

[Manage repository from a remote machine](./docs//manage_repository_from_a_remote_machine.md)

## Arborescence

The arborescence is following the defaults of [Snowfall/lib](https://snowfall.org/guides/lib/quickstart)

```txt
├── docs/architecture.puml              # overview
│
├── maskfile.md                         # entrypoint for all commands (which trigger Nix stuffs)
├── flake.nix                           # entrypoint for all Nix stuffs
│
├── systems/                            # NixOS's configuration
│  ├── hardware/                            # hardware specifics
│  ├── package/                             # system-wide packages
│  ├── service/                             # system-wide services
│  ├── networking/                          # networking specifics
│  ├── user/                                # real users definitions
│  ├── machine/fairphone_4/                     # FairPhone 4        with Nix On Droid and Home Manager
│  └── (aarch64|x86_64)-(linux|darwin)/     # machine specifics that gather all of the above
│     ├── dell-optiplex-3050/                   # Dell Optiplex 3050 with NixOS
│     ├── framework-16/                         # Framework 16       with NixOS        and Home Manager
│     ├── gigabyte-sabre-15/                    # Gigabyte Sabre 15  with NixOS        and Home Manager
│     ├── MacBook-Pro-M2/                       # Mac OS X M2        with Nix Darwin   and Home Manager
│     ├── raspberry-pi-3b-black/                # Raspberry Pi 3b    with NixOS
│     └── raspberry-pi-3b-white/                # Raspberry Pi 3b    with NixOS
│
├── homes/                              # user-wide configuration with Nix
│  ├── config.nix                           # `nix`'s user-wide configuration
│  ├── home.nix                             # home-manager's configuration
│  ├── package/                             # user-wide packages
│  └── service/                             # user-wide services
│
├── modules/                            # modules loaded automatically
│  ├── nixos/                               # on all NixOS configurations
│  └── home/                                # on all Home Manager configurations
│
├── packages/                           # programs that are packaged with Nix but not packaged elsewhere
│
├── shells/                             # shells with needed packages to execute commands
│
├── overlays/                           # used Nix overlays
│
├── nix_cache/                          # cache used by Nix
│
├── lib/                                # some usefull functions for Nix
│
├── dotfiles/                           # all files that are symlinked to home directory
│  ├── config/
│  └── local/
└── manual_install/
   ├── link-dotfiles.sh                     # executable that symlink files to home directory
   ├── install.conf.yaml                    # map local path to home path to make symlinks
   │
   └── fish/install.fish                    # executable that install Fish's plugins
```

## Packages

There are [some packages](./packages/) that i have manually packaged with [Nix](https://nixos.org/)

You can submit them to [nixpkgs](https://github.com/NixOS/nixpkgs), please [let me know](https://gitlab.com/pinage404/dotfiles/-/issues)

Read [the documentation how to use packages](./docs/packages.md#using-packages)

[Some packages have been upstreamed](https://github.com/NixOS/nixpkgs/pulls?q=is%3Apr+author%3Apinage404) and are no longer repackaged in this repository

## Story

Discover [the story](./story.md) behind this repository

## Overview

[Overview](./docs/overview.puml)

![Overview](./docs/main.svg)

_Generated with [nix-topology](https://oddlama.github.io/nix-topology/)_

## Licence

[WTFPL-2.0](LICENCE)
