{ pkgs, ... }:

let
  fira-code = pkgs.nerdfonts.override {
    fonts = [
      "FiraCode"
    ];
  };
in
{
  fonts = {
    enableDefaultPackages = true;
    packages = [
      fira-code
    ];
    fontDir.enable = true;
    fontconfig.defaultFonts = {
      monospace = [
        "FiraCode Nerd Font"
      ];
    };
  };
  console = {
    # broken
    # https://github.com/NixOS/nixpkgs/issues/125521
    # font = "fira-code";
    packages = [
      fira-code
    ];
  };
}
