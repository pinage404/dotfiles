{
  networking.computerName = "MacBook-Pro-M2";
  networking.hostName = "MacBook-Pro-M2";
  networking.localHostName = "MacBook-Pro-M2";

  networking.dns = import ../../networking/custom_dns_domain_name_server.nix;
  # trace: warning: networking.knownNetworkServices is empty, dns servers will not be configured.
  networking.knownNetworkServices = [
    "Wi-Fi"
    "USB 10/100/1000 LAN"
    "Thunderbolt Bridge"
  ];
}
