{
  pkgs,
  ...
}:

{
  imports = [
    ./networking.nix
    ./mac_os_x_better_behaviors.nix
    ../../service/nix_garbage_collector.nix
    ../../../nix_cache/cache.nix
    ../../nix/nixPath.nix
    ../../user/eb.nix
    ../../user/invited.nix

    ../../home-manager.nix
    {
      home-manager.users.eb = {
        imports = [
          (../. + "./../../homes/aarch64-darwin/eb@MacBook-Pro-M2/default.nix")
        ];
      };
      home-manager.users.invited = {
        imports = [
          (../. + "./../../homes/aarch64-darwin/invited@MacBook-Pro-M2/default.nix")
        ];
      };
    }
  ];

  nix.settings.trusted-users = [
    "eb"
  ];

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = [
    pkgs.vim
  ];

  nix.settings.experimental-features = "nix-command flakes";

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/home-manager/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/home-manager/darwin/configuration.nix";

  nix.enable = false;
  # Auto upgrade nix package and the daemon service.
  # services.nix-daemon.enable = true;
  # nix.package = pkgs.nix;

  nix.daemonIOLowPriority = true;

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true; # default shell on catalina
  programs.fish.enable = true;

  programs.nix-index.enable = true; # provide command-not-found

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
