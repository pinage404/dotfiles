{
  # user interface
  system.defaults.NSGlobalDomain.AppleInterfaceStyle = "Dark";
  system.defaults.NSGlobalDomain.AppleShowScrollBars = "Automatic";

  system.defaults.NSGlobalDomain.NSDisableAutomaticTermination = true;
  system.defaults.NSGlobalDomain.NSDocumentSaveNewDocumentsToCloud = false;
  # system.defaults.NSGlobalDomain.com.apple.keyboard.fnState = true; # in the documentation, but fails

  system.defaults.dock.autohide = true;
  system.defaults.dock.mru-spaces = false;

  # actions on corners
  system.defaults.dock.wvous-tl-corner = 2; # Mission Control
  system.defaults.dock.wvous-tr-corner = 1; # Disabled
  system.defaults.dock.wvous-bl-corner = 11; # Launchpad
  system.defaults.dock.wvous-br-corner = 4; # Desktop

  # better file explorer
  system.defaults.NSGlobalDomain.AppleShowAllExtensions = true;
  system.defaults.NSGlobalDomain.AppleShowAllFiles = true;
  system.defaults.finder.AppleShowAllExtensions = true; # display files extensions
  system.defaults.finder.AppleShowAllFiles = true;
  system.defaults.finder.ShowPathbar = true;
  system.defaults.finder.ShowStatusBar = true;
  system.defaults.finder.QuitMenuItem = true;

  # mouse and trackpad
  system.defaults.NSGlobalDomain.AppleEnableMouseSwipeNavigateWithScrolls = false;
  system.defaults.NSGlobalDomain.AppleEnableSwipeNavigateWithScrolls = false;
  system.defaults.NSGlobalDomain."com.apple.swipescrolldirection" = false; # scroll in the normal direction (the same direction as default on Linux and on windows)
  system.defaults.trackpad.ActuationStrength = 0; # 0 to enable Silent Clicking, 1 to disable. The default is 1.
  system.defaults.NSGlobalDomain."com.apple.mouse.tapBehavior" = 1; # tap to click
  system.defaults.trackpad.Clicking = true; # tap to click

  # system.defaults.universalaccess.reduceTransparency = true; # in the documentation, but fails

  system.defaults.loginwindow.GuestEnabled = false;

  security.pam.enableSudoTouchIdAuth = true; # unlock sudo with fingerprint
  system.defaults.screensaver.askForPassword = true;
  system.defaults.screensaver.askForPasswordDelay = 10;
}
