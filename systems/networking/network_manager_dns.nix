{ config, ... }:

{
  networking.networkmanager.appendNameservers = config.networking.nameservers;
}
