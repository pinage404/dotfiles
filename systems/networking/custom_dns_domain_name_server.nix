[
  # Domain Name Servers provided by the French Data Network https://www.fdn.fr/actions/dns/
  "80.67.169.12"
  "2001:910:800::12"
  "80.67.169.40"
  "2001:910:800::40"
]
