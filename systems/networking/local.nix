{
  networking = {
    defaultGateway = "192.168.1.1";
    nameservers = [
      "192.168.1.100" # AdGuard Home
      "192.168.1.1" # box
    ];
  };
}
