{
  networking.hosts = {
    "192.168.1.37" = [
      "fairphone_4.local"
    ];
    "192.168.1.100" = [
      "raspberry-pi-3b-black.local"
      "pi3bb.local"
      "adguard-home.local"
    ];
    "192.168.1.101" = [
      "raspberry-pi-3b-white.local"
      "pi3bw.local"
      "seedbox.local"
      "torrent.local"
      "deluge.local"
      "nas.local"
      "nextcloud.local"
      "media-server.local"
    ];
    "192.168.1.102" = [
      "dell-optiplex-3050.local"
      "optiplex.local"
      "media-player.local"
    ];
  };
}
