{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # know which graphic card is used
    # graphic card used by default
    # `glxinfo | egrep "OpenGL vendor|OpenGL renderer"`
    # graphic card used with nvidia offload
    # `nvidia-offload glxinfo | egrep "OpenGL vendor|OpenGL renderer"`
    glxinfo
  ];

  boot.kernelPackages = pkgs.unfree.linuxPackages;

  hardware.nvidia = {
    prime = {
      offload.enable = true;

      # Bus ID of the Intel GPU. You can find it using lspci, either under 3D or VGA
      intelBusId = "PCI:00:02:0";

      # Bus ID of the NVIDIA GPU. You can find it using lspci, either under 3D or VGA
      nvidiaBusId = "PCI:01:00:0";
    };

    modesetting = {
      enable = true;
    };
  };
}
