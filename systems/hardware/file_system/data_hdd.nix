{
  fileSystems."/media/data_hdd" = {
    device = "/dev/disk/by-label/data_hdd";
    options = [
      "defaults"
      "noauto" # do not mount on boot
      "x-systemd.automount" # mount when needed
      "x-systemd.idle-timeout=5m" # unmount after 5 min of inactivity
    ];
  };
}
