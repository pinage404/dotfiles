{
  fileSystems."/media/data_hdd/media" = {
    device = "nas.local:/media/data_hdd/media";
    fsType = "nfs4";
    noCheck = true;
    options = [
      # should contains only media
      "ro" # read-only
      "noexec" # media shouldn't execute anything

      # shared through network
      "noauto" # do not mount on boot
      "_netdev" # used to prevent the system from attempting to mount these filesystems until the network has been enabled on the system
      "x-systemd.automount" # mount when needed
      "x-systemd.idle-timeout=5m" # unmount after 5 min of inactivity
    ];
  };
}
