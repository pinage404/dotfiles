{
  config,
  ...
}:

{
  fileSystems."/media/game" = {
    device = "/dev/disk/by-label/game";
    noCheck = true;
    options = [
      "defaults"
      "noauto" # do not mount on boot
      "x-systemd.automount" # mount when needed
      "x-systemd.idle-timeout=5m" # unmount after 5 min of inactivity

      "user_id=${toString config.users.users.pinage404.uid}"
      "group_id=${toString config.users.groups.pinage404.gid}"
      "uid=${toString config.users.users.pinage404.uid}"
      "gid=${toString config.users.groups.pinage404.gid}"
    ];
  };
}
