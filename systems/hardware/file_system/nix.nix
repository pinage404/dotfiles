{
  # Before you have to [move `/nix` to another partition](/README.md#move-nix-to-another-partition)
  fileSystems."/nix" = {
    device = "/dev/disk/by-label/nix";
    fsType = "ext4";
    neededForBoot = true;
    # This will reduce metadata writes, improving I/O and the device's lifespan.
    options = [ "noatime" ];
  };
}
