{
  inputs,
  ...
}:

{
  nix.nixPath = [
    "nixpkgs=${inputs.nixpkgs-unstable}" # pin nixpkgs to nixpkgs-unstable in order to make nix-index works without channel https://github.com/nix-community/nix-index/issues/167#issuecomment-1152781614
  ];
}
