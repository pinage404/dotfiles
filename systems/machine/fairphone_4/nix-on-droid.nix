{
  pkgs,
  ...
}:

{
  # https://nix-community.github.io/nix-on-droid/nix-on-droid-options.html

  imports = [
    # can't re-use because
    # nix-on-droid
    #   environment.packages
    # nixos
    #   environment.systemPackages
    # ../../package/cli/git.nix
    # ../../package/cli/admin.nix
    # ../../package/cli/tools.nix
    # ../../package/cli/network.nix
    # ../../package/cli/fish.nix

    ../../networking/hosts.nix
    # ../../nix_flake.nix
    ../../home-manager.nix

    # can't re-use because https://github.com/nix-community/nix-on-droid/issues/166#issue-1091052301
    # nix-on-droid
    #   nix.substituters
    #   nix.trustedPublicKeys
    # nixos
    #   nix.settings.substituters
    #   nix.settings.trusted-public-keys
    # ../../../nix_cache/cache.nix

    ../../../modules/nix-on-droid/sshd/default.nix
    ./sshd.nix

    # ../../i18n.nix
    ../../time.nix
    # ../../fonts.nix
  ];

  android-integration.am.enable = true;
  android-integration.termux-open-url.enable = true;
  android-integration.xdg-open.enable = true;

  # Simply install just the packages
  environment.packages = with pkgs; [
    nano
    git
  ];

  home-manager.config = ./home.nix;

  # Set up nix for flakes
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # Read the changelog before changing this value
  system.stateVersion = "24.05";
}
