{
  services.openssh = {
    enable = true;
    ports = [ 8022 ];
    authorizedKeysFiles = [
      ../../../dotfiles/ssh/keys/local.pub
    ];
  };
}
