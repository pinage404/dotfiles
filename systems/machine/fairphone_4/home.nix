{
  pkgs,
  ...
}:

{
  imports = [
    ../../../homes/package/cli/admin.nix
    ../../../homes/package/cli/cli.nix
    # ../../../homes/package/cli/dev.nix
    # ../../../homes/package/cli/misc.nix
    # ../../../homes/package/cli/nix.nix
    ../../../homes/package/cli/system.nix
    # ../../../homes/package/cli/xdg.nix

    # ../../../homes/package/gui/admin.nix
    # ../../../homes/package/gui/cli.nix
    # ../../../homes/package/gui/dev.nix
    # ../../../homes/package/gui/misc.nix
    # ../../../homes/package/gui/system.nix

    ../../../homes/service/home_manager_auto_upgrade.nix
    ../../../homes/service/nix_garbage_collector.nix
    ../../../homes/fonts.nix
    ../../../homes/l10n.nix

    # can't re-use because https://github.com/nix-community/nix-on-droid/issues/166#issue-1091052301
    # nix-on-droid
    #   nix.substituters
    #   nix.trustedPublicKeys
    # home manager
    #   nix.settings.substituters
    #   nix.settings.trusted-public-keys
    ../../../nix_cache/cache.nix
  ];

  nix.package = pkgs.nix;

  home.packages = [
    pkgs.nix
  ];

  home.stateVersion = "24.05";
}
