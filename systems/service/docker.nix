{
  virtualisation.docker = {
    enable = true;
    liveRestore = false;
    rootless.enable = true;
  };

  virtualisation.containerd = {
    enable = true;
    nixSnapshotterIntegration = true;
  };
}
