{
  services.journald.extraConfig = ''
    SystemKeepFree=100M
    MaxRetentionSec=2months
  '';

  systemd = {
    timers.journald-keep-free-space = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "Sun 14:00:00";
        Persistent = true;
      };
    };

    services.journald-keep-free-space = {
      description = "Remove journal files older than specified time";
      script = "journalctl --vacuum-time=2months";
      serviceConfig = {
        Type = "oneshot";
      };
    };
  };
}
