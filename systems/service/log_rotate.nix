{
  services.logrotate.settings = {
    coredump = {
      files = [ "/var/lib/systemd/coredump/**" ];
    };
  };
}
