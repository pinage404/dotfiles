{
  services.xserver = {
    enable = true;

    desktopManager.kodi.enable = true;
  };

  services.displayManager = {
    lightdm.enable = true;

    autoLogin = {
      enable = true;
      user = "pi";
    };
  };
}
