{
  # Enable touchpad support
  services.libinput.touchpad = {
    sendEventsMode = "enabled";
    disableWhileTyping = true;
  };

  # multitouch gestures recognition
  services.touchegg.enable = true;
}
