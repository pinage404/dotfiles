{
  pkgs,
  ...
}:

{
  virtualisation.podman.enable = true;

  environment.etc."/etc/containers/policy.json".source = "${pkgs.skopeo.policy}/default-policy.json";
}
