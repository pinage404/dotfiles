{
  pkgs,
  ...
}:

{
  services.ollama.enable = true;
  services.ollama.package = pkgs.unstable.ollama;
  services.ollama.acceleration = "rocm";
  services.ollama.rocmOverrideGfx = "11.0.2";

  systemd.services.ollama.serviceConfig = {
    TimeoutStopSec = 10;
  };

  services.open-webui.enable = true;
  services.open-webui.package = pkgs.unstable.open-webui;
  services.open-webui.environment = {
    ANONYMIZED_TELEMETRY = "False";
    DO_NOT_TRACK = "True";
    SCARF_NO_ANALYTICS = "True";
    WEBUI_URL = "http://localhost:8080";
  };
}
