{
  lib,
  ...
}:

{
  # copied from https://github.com/nix-community/srvos/blob/cd802e2933c567ea91de48dbe8968f41a5d9a642/nixos/common/nix.nix
  # nix.settings.connect-timeout = 5; # make update fails with the following error
  # warning: error: unable to download 'https://api.github.com/repos/nixos/nixpkgs/commits/nixos-24.11': Timeout was reached (28); retrying in 326 ms

  # The default at 10 is rarely enough
  nix.settings.log-lines = lib.mkDefault 25;

  # stop freezing when upgrading / downloading new packages ...
  nix = {
    daemonCPUSchedPolicy = lib.mkDefault "idle";
    daemonIOSchedClass = lib.mkDefault "idle";
    daemonIOSchedPriority = lib.mkDefault 7;
  };

  # Make builds to be more likely killed than important services
  # 100 is the default for user slices and 500 is systemd-coredumpd@
  # We rather want a build to be killed than our precious user sessions as builds can be easily restarted
  systemd.services.nix-daemon.serviceConfig.OOMScoreAdjust = lib.mkDefault 250;

  # Avoid copying unnecessary stuff over SSH
  nix.settings.builders-use-substitutes = true;
}
