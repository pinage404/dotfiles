{
  config,
  lib,
  ...
}:

let
  block_lists = {
    "AdGuard DNS filter" = "https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt";
    "AdAway Default Blocklist" = "https://adaway.org/hosts.txt";
    "Phishing URL Blocklist" =
      "https://malware-filter.gitlab.io/malware-filter/phishing-filter-agh.txt";
    "Malicious URL Blocklist" =
      "https://malware-filter.gitlab.io/malware-filter/urlhaus-filter-agh.txt";
    "EasyList" = "https://easylist.to/easylist/easylist.txt";
    "EasyPrivacy" = "https://easylist.to/easylist/easyprivacy.txt";
    "The Big List of Hacked Malware Web Sites" =
      "https://raw.githubusercontent.com/mitchellkrogza/The-Big-List-of-Hacked-Malware-Web-Sites/master/hosts";
    "NoCoin adblock list" =
      "https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt";
  };
in
{
  services.adguardhome = {
    enable = true;
    openFirewall = true;
    mutableSettings = false;
    settings = {
      dns = {
        bind_hosts = [ "0.0.0.0" ];
        bootstrap_dns = config.networking.nameservers;
        upstream_dns = config.networking.nameservers;
      };
      querylog = {
        enabled = true;
        interval = "${toString (24 * 7)}h";
      };
      statistics = {
        enabled = true;
        interval = "${toString (24 * 7)}h";
      };

      filters = lib.mapAttrsToList (name: url: {
        enabled = true;
        inherit url;
        inherit name;
      }) block_lists;
    };
  };

  # ports from https://github.com/AdguardTeam/AdGuardHome/blob/aaaa56fce3cfea711bab3584e00f6d47acea6c02/scripts/make/Dockerfile#L39
  networking.firewall = {
    allowedTCPPorts = [
      53 # DNS
      80 # HTTP
      443 # HTTPS, DNS-over-HTTPS (incl. HTTP/3), DNSCrypt (main)
      853 # DNS-over-TLS, DNS-over-QUIC
      3000 # HTTP(S) (alt, incl. HTTP/3)
      3001 # HTTP(S) (beta, incl. HTTP/3)
      5443 # DNSCrypt (alt)
      6060 # HTTP (pprof)
      8853 # DNS-over-QUIC
    ];
    allowedUDPPorts = [
      53 # DNS
      67 # DHCP (server)
      68 # DHCP (client)
      443 # HTTPS, DNS-over-HTTPS (incl. HTTP/3), DNSCrypt (main)
      784 # DNS-over-QUIC
      853 # DNS-over-TLS, DNS-over-QUIC
      3000 # HTTP(S) (alt, incl. HTTP/3)
      3001 # HTTP(S) (beta, incl. HTTP/3)
      5443 # DNSCrypt (alt)
      8853 # DNS-over-QUIC
    ];
  };
}
