{ config, ... }:

{
  system.autoUpgrade = {
    enable = true;

    dates = "15:30";
    flake = "gitlab:pinage404/dotfiles?ref=main#${config.networking.hostName}";
  };

  systemd.services.nixos-upgrade = {
    serviceConfig = {
      Nice = 16;
    };
  };
}
