{
  services.nfs.server = {
    enable = true;

    exports = ''
      /media/data_hdd/media media-player.local()
    '';
  };

  networking.firewall.allowedTCPPorts = [ 2049 ];
}
