{
  # SDDM (Simple Desktop Display Manager) the default KDE display manager
  services.displayManager.sddm = {
    enable = true;
    autoNumlock = true;
  };
}
