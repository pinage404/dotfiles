{ config, pkgs, ... }:

let
  nextcloud_home_path = "/media/data_hdd/cloud/nextcloud";
in
{
  services.nextcloud = {
    enable = true;
    package = pkgs.nextcloud20;

    # home's option is broken https://github.com/NixOS/nixpkgs/pull/81623
    # home = "/home/nextcloud";

    hostName = "nextcloud.local";

    config = {
      adminpass = builtins.readFile ./nextcloud_admin.pass;
      extraTrustedDomains = [
        "nextcloud.local"
        (builtins.head config.networking.interfaces.eth0.ipv4.addresses).address
      ];
    };
  };

  networking.firewall = {
    allowedTCPPorts = [ 80 ];
  };

  # move NextCloud's home to HDD to avoid filling SD card
  systemd.services.nextcloud-setup = {
    after = [ "media-data_hdd.mount" ];
    requires = [ "media-data_hdd.mount" ];
    preStart = ''
      for DIR in "apps" "config" "data" "store-apps"; do
        mkdir --parents ${nextcloud_home_path}/$DIR
        chown --recursive nextcloud:nextcloud ${nextcloud_home_path}/$DIR
        ln --symbolic --force ${nextcloud_home_path}/$DIR ${config.services.nextcloud.home}
      done
    '';
  };
}
