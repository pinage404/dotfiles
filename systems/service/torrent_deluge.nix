{ lib, pkgs, ... }:

let
  deluge_data_dir = "/home/deluge";

  deluge_config_dir = "${deluge_data_dir}/.config/deluge/";
  dotfiles_deluge_config_dir = "/home/pi/dotfiles/dotfiles/config/deluge/";
  deluge_config_files_name = [
    "core.conf"
    "web.conf"
    "ui.conf"
    "autoadd.conf"
    "blocklist.conf"
    "extractor.conf"
    "scheduler.conf"
  ];
  deluge_config_files = map (
    name: pkgs.writeText name (builtins.readFile (dotfiles_deluge_config_dir + name))
  ) deluge_config_files_name;
  deluge_config_core = builtins.fromJSON (
    builtins.readFile "${dotfiles_deluge_config_dir}/core.conf"
  );
  deluge_auth_file = ./torrent_deluge.authFile;

  deluge_download_dir = "${deluge_data_dir}/Download";
  download_dir = "/media/data_hdd/Download";
in
{
  imports = [
    ../user/data-manager.nix
  ];

  services.deluge = {
    enable = true;
    dataDir = deluge_data_dir;

    group = "data-manager";

    declarative = true;
    config = deluge_config_core;
    authFile = deluge_auth_file;

    openFirewall = true;

    web = {
      enable = true;
      openFirewall = true;
    };
  };

  networking.firewall = {
    allowedTCPPorts = [
      deluge_config_core.daemon_port
    ];
  };

  systemd.services.deluged = {
    after = [ "media-data_hdd.mount" ];
    requires = [ "media-data_hdd.mount" ];
    preStart = lib.mkForce (
      ''
        mkdir --parents ${deluge_data_dir}

        mkdir --parents ${deluge_config_dir}
        ln --symbolic --backup --force ${deluge_auth_file} ${deluge_config_dir}/auth

        # use Deluge's config's files from dotfiles
        for config_file in '${(lib.concatStringsSep "' '" deluge_config_files)}'; do
          cp --force $config_file ${deluge_config_dir}/$''
      + ''
        {config_file#*-}
                done

                # move Deluge's Download folder to HDD to avoid filling SD card
                mkdir --parents ${download_dir}
                rmdir ${deluge_download_dir} || rm --force ${deluge_download_dir}
                ln --symbolic --force ${download_dir} ${deluge_data_dir}
      ''
    );
  };
}
