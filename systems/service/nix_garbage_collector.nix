{
  lib,
  pkgs,
  ...
}:

{
  nix = {
    # free Nix store when full
    settings = {
      min-free = "100M";
      max-free = "5G";
    };

    # https://nixos.wiki/wiki/Storage_optimization#Optimising_the_store
    settings.auto-optimise-store = pkgs.stdenv.hostPlatform.isLinux; # maybe a source of error on Mac https://github.com/NixOS/nix/issues/7273#issue-1438740747 ?

    # https://nixos.wiki/wiki/Storage_optimization#Automation
    gc =
      {
        # automatic = true;
        options = "--delete-older-than 30d";
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        dates = "Sunday, 15:00";
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isDarwin {
        interval = {
          Hour = 12;
          Minute = 30;
        };
      };

    optimise =
      {
        # automatic = true;
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        dates = [ "Sunday, 16:00" ];
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isDarwin {
        interval = {
          Hour = 12;
          Minute = 45;
        };
      };
  };
}
