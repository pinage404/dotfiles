{
  config,
  lib,
  pkgs,
  ...
}:

let
  cache_path = "${config.users.users.spotifyd.home}/.cache/spotifyd/";
  configuration_file_name = "spotifyd.conf";
  toml = pkgs.formats.toml { };
  deepMerge = listOfObjects: (builtins.foldl' lib.attrsets.recursiveUpdate { } listOfObjects);
in
{
  age.secrets."spotifyd".file = ../../secrets/spotifyd.age;
  age.secrets."spotifyd".mode = "0440";
  age.secrets."spotifyd".owner = config.users.users.spotifyd.name;
  age.secrets."spotifyd".group = config.users.groups.keys.name;

  services.spotifyd = {
    enable = true;
    settings = deepMerge [
      (lib.trivial.importTOML ./spotifyd.conf)
      {
        global = {
          inherit cache_path;
          password_cmd = config.age.secrets."spotifyd".path;
        };
      }
    ];
  };

  # don't use SpotifyD default service from nixpkgs because cache's directory is forced
  systemd.services.spotifyd.serviceConfig.ExecStart =
    lib.mkForce "${lib.getExe pkgs.spotifyd} --no-daemon";

  # put configuration file on the default location for SpotifyD as per the documentation https://github.com/Spotifyd/spotifyd/blob/357e1476fd4e987b82d00ac7b70891d1730509e8/docs/src/config/File.md?plain=1#L3
  environment.etc."${configuration_file_name}".source =
    toml.generate configuration_file_name config.services.spotifyd.settings;

  users = {
    users.spotifyd = {
      name = "spotifyd";
      isSystemUser = true;
      home = "/home/spotifyd";
      createHome = true;
      description = "spotifyd";
      group = "spotifyd";
      shell = pkgs.bash;
      extraGroups =
        (lib.optional config.hardware.pulseaudio.systemWide "audio") # to be able to play sound with PulseAudio
        ++ (lib.optional config.services.pipewire.systemWide "pipewire") # to be able to play sound with Pipewire
      ;
    };
    groups.spotifyd = {
      name = "spotifyd";
    };
  };

  environment.systemPackages = with pkgs; [
    spotifyd
  ];

  systemd.services.spotifyd_cache_folder = {
    before = [ "spotifyd.service" ];
    wantedBy = [ "spotifyd.service" ];
    description = "create spotifyd cache folder";
    script = ''
      mkdir --parents "${cache_path}"
      chown --recursive ${toString config.users.users.spotifyd.name}:${toString config.users.groups.spotifyd.name} "${cache_path}"
    '';
  };
}
