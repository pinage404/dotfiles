{
  config,
  pkgs,
  lib,
  ...
}:

let
  secrets_config = {
    mode = "0440";
    owner = "gitlab-runner";
    group = config.users.groups.keys.name;
  };
in
{
  age.secrets."gitlab-runner/default" = secrets_config;
  age.secrets."gitlab-runner/default-2" = secrets_config;
  age.secrets."gitlab-runner/default-3" = secrets_config;
  age.secrets."gitlab-runner/nix" = secrets_config;

  services.gitlab-runner = {
    enable = true;

    gracefulTermination = true;
    gracefulTimeout = "10s";

    services = {
      default = {
        # File should contain at least these two variables:
        # ```env
        # CI_SERVER_URL=https://gitlab.com
        # CI_SERVER_TOKEN=my_secret_token_generated_by_gitlab
        # ```
        authenticationTokenConfigFile = config.age.secrets."gitlab-runner/default".path;
        dockerImage = "ruby:2.7";
        limit = 7;
        requestConcurrency = 7;
      };

      default-2 = {
        # File should contain at least these two variables:
        # ```env
        # CI_SERVER_URL=https://gitlab.com
        # CI_SERVER_TOKEN=my_secret_token_generated_by_gitlab
        # ```
        authenticationTokenConfigFile = config.age.secrets."gitlab-runner/default-2".path;
        dockerImage = "ruby:2.7";
        limit = 7;
        requestConcurrency = 7;
      };

      default-3 = {
        # File should contain at least these two variables:
        # ```env
        # CI_SERVER_URL=https://gitlab.com
        # CI_SERVER_TOKEN=my_secret_token_generated_by_gitlab
        # ```
        authenticationTokenConfigFile = config.age.secrets."gitlab-runner/default-3".path;
        dockerImage = "ruby:2.7";
        limit = 7;
        requestConcurrency = 7;
      };

      # runner for building in docker via host's nix-daemon
      # nix store will be readable in runner, might be insecure
      nix = {
        # File should contain at least these two variables:
        # ```env
        # CI_SERVER_URL=https://gitlab.com
        # CI_SERVER_TOKEN=my_secret_token_generated_by_gitlab
        # ```
        authenticationTokenConfigFile = config.age.secrets."gitlab-runner/nix".path;

        dockerImage = "alpine";
        dockerVolumes = [
          "/nix/store:/nix/store:ro"
          "/nix/var/nix/db:/nix/var/nix/db:ro"
          "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
        ];
        dockerDisableCache = true;
        preBuildScript = pkgs.writeScript "setup-container" ''
          mkdir -p -m 0755 /nix/var/log/nix/drvs
          mkdir -p -m 0755 /nix/var/nix/gcroots
          mkdir -p -m 0755 /nix/var/nix/profiles
          mkdir -p -m 0755 /nix/var/nix/temproots
          mkdir -p -m 0755 /nix/var/nix/userpool
          mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
          mkdir -p -m 1777 /nix/var/nix/profiles/per-user
          mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
          mkdir -p -m 0700 "$HOME/.nix-defexpr"

          . ${pkgs.nix}/etc/profile.d/nix.sh

          ${lib.getExe' pkgs.nix "nix-channel"} --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
          ${lib.getExe' pkgs.nix "nix-channel"} --update nixpkgs
          ${lib.getExe' pkgs.nix "nix-env"} --install ${
            lib.concatStringsSep " " (
              with pkgs;
              [
                nix
                cacert
                git
                openssh
              ]
            )
          }
        '';
        environmentVariables = {
          ENV = "/etc/profile";
          USER = "root";
          NIX_REMOTE = "daemon";
          PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
          NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
        };
      };
    };
  };

  systemd.services.gitlab-runner = {
    serviceConfig = {
      Nice = 15;
    };
  };
}
