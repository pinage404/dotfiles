{
  services.xserver = {
    # Enable the X11 windowing system
    enable = true;

    # Enable C-M-Bksp to kill X
    enableCtrlAltBackspace = true;
  };
}
