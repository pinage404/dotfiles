{
  # Enable the OpenSSH daemon
  services.openssh = {
    enable = true;
    startWhenNeeded = true;
    settings.X11Forwarding = true;
  };
  services.sshguard.enable = true;
  systemd.services.sshguard.serviceConfig = {
    TimeoutStopSec = 10;
  };
}
