{
  imports = [
    ../user/data-manager.nix
  ];

  services.jellyfin = {
    enable = true;
    openFirewall = true;
    group = "data-manager";
  };
}
