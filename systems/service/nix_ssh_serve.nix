{
  nix.sshServe.enable = true;

  nix.sshServe.write = true;
  nix.settings.trusted-users = [
    "root"
    "nix-ssh"
    "pi"
  ];
}
