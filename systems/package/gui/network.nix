{ pkgs, ... }:

{
  # Allow wireshark to be used with a non-root user
  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark;
  };

  environment.systemPackages = with pkgs; [
    networkmanager
  ];
}
