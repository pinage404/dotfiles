{
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
  };

  hardware.xpadneo.enable = true; # support XBox controller

  programs.gamemode.enable = true;
}
