{
  # Needed by VSCode's authentification
  programs.dconf.enable = true;
  services.gnome = {
    gnome-keyring.enable = true;
    gnome-online-accounts.enable = true;
  };

  # Needed to allow direct (peer-to-peer / P2P) connection with VSCode Live Share
  # https://docs.microsoft.com/en-us/visualstudio/liveshare/reference/connectivity#manually-adding-a-firewall-entry
  networking.firewall = {
    allowedTCPPortRanges = [
      {
        from = 5990;
        to = 5999;
      }
    ];
    allowedTCPPorts = [
      3030 # slidev
    ];
  };
}
