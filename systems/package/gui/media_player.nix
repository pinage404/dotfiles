{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    vlc
    electronplayer
    jellyfin-media-player
    (kodi.withPackages (
      kodiPackages: with kodiPackages; [
        keymap
        youtube
        netflix
        jellyfin
        trakt
        arteplussept
        a4ksubtitles
      ]
    ))
    unfree.spotify
    popcorntime
  ];
}
