{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Partitions tools
    gparted

    firmware-manager
    firmware-updater
  ];
}
