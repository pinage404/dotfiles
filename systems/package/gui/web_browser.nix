{
  pkgs,
  ...
}:

{
  programs.firefox.enable = true;
  programs.firefox.nativeMessagingHosts.packages = [ pkgs.fx-cast-bridge ];
  programs.firefox.languagePacks = [
    "fr"
    "en-GB"
  ];
}
