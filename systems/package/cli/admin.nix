{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    sudo
    killall

    # Partitions tools
    gnufdisk
    parted
    testdisk
    fuse
    sshfs
    ntfs3g
    dosfstools
    lsof

    # Hardware tools
    usbutils
    lshw
    pciutils
    lm_sensors
    lsb-release
    upower

    # Top
    sysstat
    htop
    iftop
    iotop
    powertop
    agenix
  ];
}
