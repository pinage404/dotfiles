{
  pkgs,
  ...
}:

{
  environment.systemPackages = with pkgs; [
    nix-bash-completions

    # program that help to manage user's programs the same way as /etc/nixos/configuration.nix
    home-manager

    comma # run program without installing it

    manix # tool that search and display options of NixOS / Home Manager documentation
    nixos-option # tool that list current evaluated options values

    # run unpatched program by NixOS
    # inputs.nix-alien.packages.${system}.nix-alien
    # inputs.nix-alien.packages.${system}.nix-index-update
    # nix-index
  ];
  # run unpatched program by NixOS
  # programs.nix-ld.enable = true;
}
