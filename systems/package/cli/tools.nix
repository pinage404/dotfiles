{
  pkgs,
  lib,
  ...
}:

{
  environment.systemPackages =
    (with pkgs; [
      # Download
      wget
      curl
      rsync

      # File system
      file
      tree
      which
      lsof
      inotify-tools

      # Network
      mtr

      # Diff
      diffutils

      # Security
      gnupg

      # Search
      ripgrep # rg

      # Basic command replacement with modern tools
      bat # cat
      eza # ls
      lsd # ls
      fd # find but newer / faster / better

      # modify clipboard
      xclip
      xsel
    ])
    ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux (
      with pkgs;
      [
        ncdu
      ]
    );
}
