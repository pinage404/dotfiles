{ pkgs, ... }:

{
  programs.mtr.enable = true; # Allow mtr to be used with a non-root user

  environment.systemPackages = with pkgs; [
    nettools
    iftop
    mtr
  ];
}
