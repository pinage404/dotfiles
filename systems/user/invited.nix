# passwd invited
{
  config,
  pkgs,
  lib,
  ...
}:

{
  users = {
    groups.invited = { };
    users.invited =
      {
        home = lib.internal.get_user_home pkgs "invited";
        createHome = true;
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        isNormalUser = true;
        group = "invited";
        extraGroups = lib.internal.make_main_normal_user_extra_groups config;
        initialPassword = "invited";
      };
  };
}
