# passwd eb
{
  config,
  lib,
  pkgs,
  ...
}:

{
  users = {
    groups.eb = { };
    users.eb =
      {
        home = lib.internal.get_user_home pkgs "eb";
        createHome = true;

        openssh.authorizedKeys.keyFiles = [
          ../../dotfiles/ssh/keys/local.pub
        ];
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        isNormalUser = true;
        group = "eb";
        extraGroups = lib.internal.make_main_normal_user_extra_groups config;
        initialPassword = "eb";
      };
  };
}
