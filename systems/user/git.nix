{
  pkgs,
  lib,
  ...
}:

{
  users = {
    groups.git = { };
    users.git =
      {
        home = lib.internal.get_user_home pkgs "git";
        createHome = true;
        shell = "${lib.getExe' pkgs.git "git-shell"}";
        packages = [ pkgs.git ];
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        isNormalUser = true;
        group = "git";
        initialPassword = "git";
      };
  };
}
