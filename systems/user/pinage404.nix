# passwd pinage404
{
  config,
  pkgs,
  lib,
  ...
}:

{
  users = {
    groups.pinage404 = {
      gid = 1000;
    };
    users.pinage404 =
      {
        uid = 1000;
        home = lib.internal.get_user_home pkgs "pinage404";
        createHome = true;
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        isNormalUser = true;
        group = "pinage404";
        extraGroups = [
          "wheel" # to be able to use `sudo` command
        ] ++ (lib.internal.make_main_normal_user_extra_groups config);
        initialPassword = "pinage404";
      };
  };
}
