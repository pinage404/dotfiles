{
  pkgs,
  lib,
  ...
}:

{
  users.groups.nixos_configuration_manager = { };

  security.sudo.extraRules = [
    {
      groups = [ "nixos_configuration_manager" ];
      commands = [
        {
          command = "${lib.getExe pkgs.nixos-rebuild}";
          options = [ "NOPASSWD" ];
        }
      ];
    }
  ];
}
