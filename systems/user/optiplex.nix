# passwd optiplex
{
  config,
  pkgs,
  lib,
  ...
}:

{
  imports = [
    ./data-manager.nix
    ./nixos_configuration_manager.nix
  ];

  users = {
    groups.optiplex = {
      gid = 1000;
    };
    users.optiplex =
      {
        uid = 1000;
        home = lib.internal.get_user_home pkgs "optiplex";
        createHome = true;

        openssh.authorizedKeys.keyFiles = [
          ../../dotfiles/ssh/keys/local.pub
        ];
      }
      // lib.attrsets.optionalAttrs pkgs.stdenv.hostPlatform.isLinux {
        isNormalUser = true;
        group = "optiplex";
        extraGroups = [
          "wheel" # to be able to use `sudo` command
          "nixos_configuration_manager" # to be able to use `sudo nixos-rebuild` command without password
          "data-manager"
        ] ++ (lib.internal.make_main_normal_user_extra_groups config);
        initialPassword = "optiplex";
      };
  };
}
