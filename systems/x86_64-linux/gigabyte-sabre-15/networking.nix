{
  networking.hostName = "gigabyte-sabre-15";
  networking.networkmanager.enable = true;
  networking.hosts = {
    "127.0.0.1" = [
      "gigabyte-sabre-15.local"
    ];
  };
}
