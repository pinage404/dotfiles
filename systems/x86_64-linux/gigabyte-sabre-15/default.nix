# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’)

# test with
# nixos-rebuild build-vm -I nixos-config=./systems/x86_64-linux/gigabyte-sabre-15/default.nix
# nixos-rebuild build-vm --flake ".#gigabyte-sabre-15"
{
  inputs,
  ...
}:

{
  imports = [
    # inputs.nixos-hardware.nixosModules.common-cpu-intel
    # inputs.nixos-hardware.nixosModules.common-cpu-intel-kaby-lake
    # inputs.nixos-hardware.nixosModules.common-gpu-nvidia-disable
    # inputs.nixos-hardware.nixosModules.common-pc-laptop
    # inputs.nixos-hardware.nixosModules.common-pc-laptop-acpi_call
    # inputs.nixos-hardware.nixosModules.common-pc-laptop-hdd
    # inputs.nixos-hardware.nixosModules.common-pc-laptop-ssd
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./topology.nix
    ./swapfile.nix
    ./shutdown_fix.nix
    ./boot_loader.nix
    ./build_foreign_architecture.nix
    ./networking.nix
    ../../networking/hosts.nix
    ../../networking/dns_domain_name_server.nix
    ../../networking/network_manager_dns.nix
    ../../hardware/file_system/nix.nix
    ../../hardware/file_system/tmp.nix
    ../../hardware/audio.nix
    ../../hardware/audio_microphone_noise_suppression.nix
    ../../hardware/bluetooth.nix
    ../../hardware/mouse.nix
    ../../hardware/keyboard_backlight.nix
    ../../i18n.nix
    ../../time.nix
    ../../hardware/file_system/home.nix
    ../../hardware/file_system/data_hdd.nix
    ../../hardware/file_system/data_ssd.nix
    ../../hardware/file_system/exec_downloaded.nix
    ../../user/pinage404.nix
    ../../user/git.nix
    ../../service/x11.nix
    ../../service/touchpad.nix
    ../../service/simple_desktop_display_manager.nix
    #    ../../service/lxqt.nix
    ../../service/kde.nix
    ../../package/cli/git.nix
    ../../package/cli/admin.nix
    ../../package/cli/tools.nix
    ../../package/cli/network.nix
    ../../package/cli/fish.nix
    ../../package/cli/nixos.nix
    ../../package/gui/admin.nix
    # ../../package/gui/game.nix
    # ../../hardware/file_system/game.nix
    ../../package/gui/network.nix
    ../../package/gui/vscode.nix
    ../../package/gui/web_browser.nix
    ../../service/power_management.nix
    ../../service/systemd_journald_keep_free_space.nix
    ../../service/log_rotate.nix
    ../../service/docker.nix
    ../../service/podman.nix
    ../../service/ssh.nix
    # ../../service/antivirus_clamav.nix # too memory gready ; i have too many Out Of Memory with it
    ../../service/firewall.nix
    # ../../service/firewall_at_application_level_opensnitch.nix
    ../../service/kdeconnect.nix
    ../../service/virtualbox.nix
    ../../service/apparmor.nix
    ../../service/gpg_agent.nix
    ../../service/printer.nix
    ../../service/lirc_infrared_remote_control.nix
    ../../service/flatpak.nix
    ../../service/espanso.nix
    ../../fonts.nix
    ../../nix_flake.nix
    ../../../nix_cache/cache.nix
    ../../service/nix_garbage_collector.nix
    ../../service/auto_upgrade.nix
    ../../service/nix_daemon.nix
    ../../service/nix_trusted_users.nix
    ../../security/sudo_message.nix
    ../../xdg_portal.nix

    inputs.home-manager.nixosModules.home-manager
    ../../home-manager.nix
    {
      home-manager.users.pinage404 = {
        imports = [
          (../. + "./../../homes/x86_64-linux/pinage404@gigabyte-sabre-15/default.nix")
        ];
      };
      home-manager.users.git = import ../../../systems/user/git/home.nix;
    }
  ];

  virtualisation.docker.extraOptions = "--data-root /media/exec_downloaded/docker";

  # move storage to another partition
  # https://docs.oracle.com/en/operating-systems/oracle-linux/podman/configuring-podman-storage.html
  virtualisation.containers.storage.settings = {
    storage = {
      driver = "overlay";
      graphroot = "/media/exec_downloaded/containers/root/storage";
    };
  };

  # NetworkManager's nm-online kills nixos-rebuild
  # https://github.com/NixOS/nixpkgs/issues/180175#issue-1293615234
  systemd.services.NetworkManager-wait-online.enable = false;

  # without this, `nixos-rebuild switch` will fail with
  # the following new units were started: systemd-vconsole-setup.service
  # https://github.com/NixOS/nixpkgs/issues/202846#issue-1464490034
  boot.initrd.systemd.services.systemd-udevd.after = [ "systemd-modules-load.service" ];

  # Copy the NixOS configuration file and link it from the resulting system (/run/current-system/configuration.nix)
  # This is useful in case you accidentally delete configuration.nix
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release with which your system is to be compatible, in order to avoid breaking some software such as database servers
  # You should change this only after NixOS release notes say you should
  system.stateVersion = "23.05";
}
