{
  # See https://github.com/NixOS/nixpkgs/issues/40388#issuecomment-400560925
  boot.kernelParams = [
    "acpi_osi=!acpi_osi=\"Windows 2009\""
  ];
}
