{
  inputs,
  ...
}:

{
  imports = [
    inputs.nix-topology.nixosModules.default
  ];

  topology.self = {
    deviceIcon = "devices.laptop";
    hardware.info = "Gigabyte Sabre 15";
    interfaces.wifi.network = "home";
    interfaces.vboxnet0.virtual = true;
  };
}
