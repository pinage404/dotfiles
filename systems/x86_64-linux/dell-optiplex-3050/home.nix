{
  home.file = {
    ".config/autostart/jdsp-gui.desktop".text = ''
      [Desktop Entry]
      Exec=jamesdsp --tray
      Icon=jamesdsp
      Name=JamesDSP for Linux
      StartupNotify=false
      Terminal=false
      Type=Application
      Version=1.0
      X-GNOME-Autostart-Delay=6
      X-GNOME-Autostart-enabled=true
      X-KDE-autostart-after=panel
      X-KDE-autostart-phase=2
      X-MATE-Autostart-Delay=6
    '';

    ".config/htop/htoprc".source = ./dotfiles/config/htop/htoprc;

    ".config/kdedefaults/kdeglobals".source = ./dotfiles/config/kdedefaults/kdeglobals;
    ".config/kdedefaults/package".source = ./dotfiles/config/kdedefaults/package;

    ".config/kcminputrc".source = ./dotfiles/config/kcminputrc;

    ".config/plasmaparc".source = ./dotfiles/config/plasmaparc;

    ".config/kwalletrc".text = ''
      [Wallet]
      Enabled=false
    '';
  };
}
