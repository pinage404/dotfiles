{
  inputs,
  ...
}:

{
  imports = [
    inputs.nix-topology.nixosModules.default
  ];

  topology.self = {
    deviceIcon = "devices.desktop";
    hardware.info = "Dell Optiplex";
    interfaces.eth0.network = "home";
  };
}
