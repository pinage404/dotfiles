{
  networking = {
    hostName = "dell-optiplex-3050";

    interfaces.eth0.ipv4.addresses = [
      {
        address = "192.168.1.102";
        prefixLength = 24;
      }
    ];
  };
}
