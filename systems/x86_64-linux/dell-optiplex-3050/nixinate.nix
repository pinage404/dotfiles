{
  config,
  ...
}:

{
  _module.args.nixinate = {
    host = "${config.networking.hostName}.local";
    sshUser = "optiplex";
    buildOn = "remote";
    substituteOnTarget = true;
    hermetic = false;
    nixOptions = [ ];
    dryRun = false;
  };
}
