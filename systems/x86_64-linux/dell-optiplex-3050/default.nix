# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running 'nixos-help')

# test with
# nixos-rebuild build-vm -I nixos-config=./systems/x86_64-linux/dell-optiplex-3050/default.nix
# nixos-rebuild build-vm --flake ".#dell-optiplex-3050"
{
  inputs,
  ...
}:

{
  imports = [
    inputs.nixos-hardware.nixosModules.dell-optiplex-3050
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./topology.nix
    ./nixinate.nix
    ./boot_loader.nix
    ./networking.nix
    ../../networking/hosts.nix
    ../../networking/dns_domain_name_server.nix
    ../../networking/network_manager_dns.nix
    ../../networking/local.nix
    ../../hardware/file_system/tmp.nix
    ../../hardware/file_system/nas_nfs_client_media.nix
    ../../hardware/mouse.nix
    ../../hardware/audio.nix
    ../../nix_flake.nix
    ../../service/nix_garbage_collector.nix
    ../../service/auto_upgrade.nix
    ../../service/nix_daemon.nix
    ../../service/nix_trusted_users.nix
    ../../service/antivirus_clamav.nix
    ../../service/firewall.nix
    ../../service/ssh.nix
    ../../service/x11.nix
    ../../service/simple_desktop_display_manager.nix
    # ../../service/kde.nix
    ../../service/power_management.nix
    ../../service/swapspace.nix
    ../../service/systemd_journald_keep_free_space.nix
    ../../service/log_rotate.nix
    ../../service/automatic-shutdown.nix
    ../../service/spotifyd.nix
    ../../service/lirc_infrared_remote_control.nix
    ../../service/docker.nix
    ../../service/gitlab_runner.nix
    ../../package/cli/git.nix
    ../../package/cli/admin.nix
    ../../package/cli/tools.nix
    ../../package/cli/network.nix
    ../../package/gui/media_player.nix
    ../../package/gui/fcast-receiver.nix
    ../../package/gui/web_browser.nix
    ../../user/optiplex.nix
    ../../../nix_cache/cache.nix
    ../../i18n.nix
    ../../time.nix
  ];

  services.xserver.desktopManager = {
    # Enable the KDE Desktop Environment
    plasma5.enable = true;
  };

  services.pipewire.systemWide = true;

  # Enable automatic login for the user.
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "optiplex";

  age.secrets."gitlab-runner/default".file =
    ../../../secrets/gitlab-runner/dell-optiplex-3050/default.age;
  age.secrets."gitlab-runner/default-2".file =
    ../../../secrets/gitlab-runner/dell-optiplex-3050/default-2.age;
  age.secrets."gitlab-runner/default-3".file =
    ../../../secrets/gitlab-runner/dell-optiplex-3050/default-3.age;
  age.secrets."gitlab-runner/nix".file = ../../../secrets/gitlab-runner/dell-optiplex-3050/nix.age;

  # This value determines the NixOS release from which the default settings for stateful data, like file locations and database versions on your system were taken
  # It's perfectly fine and recommended to leave this value at the release version of the first install of this system
  # Before changing this value read the documentation for this option (e.g. man configuration.nix or on https://nixos.org/nixos/options.html)
  system.stateVersion = "23.11"; # Did you read the comment?
}
