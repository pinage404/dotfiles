{
  inputs,
  ...
}:

{
  imports = [
    inputs.nix-topology.nixosModules.default
  ];

  topology.self = {
    deviceIcon = "devices.laptop";
    hardware.info = "Framework 16";
    interfaces.wifi.network = "home";
    interfaces.vboxnet0.virtual = true;
  };
}
