{
  networking.hostName = "framework-16";
  networking.networkmanager.enable = true;
  networking.hosts = {
    "127.0.0.1" = [
      "framework-16.local"
    ];
  };
}
