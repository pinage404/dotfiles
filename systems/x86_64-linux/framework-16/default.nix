# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page, on https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

# test with
# nixos-rebuild build-vm -I nixos-config=./systems/x86_64-linux/framework-16/default.nix
# nixos-rebuild build-vm --flake ".#framework-16"
{
  inputs,
  config,
  ...
}:

{
  imports = [
    inputs.nixos-hardware.nixosModules.framework-16-7040-amd
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./topology.nix
    ./boot_loader.nix
    ./build_foreign_architecture.nix
    ./networking.nix
    ./game.nix
    ../../networking/hosts.nix
    ../../networking/dns_domain_name_server.nix
    ../../networking/network_manager_dns.nix
    ../../hardware/audio.nix
    ../../hardware/audio_microphone_noise_suppression.nix
    ../../hardware/bluetooth.nix
    ../../hardware/mouse.nix
    ../../i18n.nix
    ../../time.nix
    ../../user/pinage404.nix
    ../../user/git.nix
    ../../user/invited.nix
    ../../user/eb.nix
    ../../service/x11.nix
    ../../service/touchpad.nix
    ../../service/simple_desktop_display_manager.nix
    ../../service/kde.nix
    ../../package/cli/git.nix
    ../../package/cli/admin.nix
    ../../package/cli/tools.nix
    ../../package/cli/network.nix
    ../../package/cli/fish.nix
    ../../package/cli/nixos.nix
    ../../package/gui/admin.nix
    ../../package/gui/network.nix
    ../../package/gui/vscode.nix
    ../../package/gui/web_browser.nix
    ../../package/gui/appimage.nix
    ../../service/power_management.nix
    ../../service/swapspace.nix
    ../../service/systemd_journald_keep_free_space.nix
    ../../service/log_rotate.nix
    ../../service/docker.nix
    ../../service/podman.nix
    ../../service/gitlab_runner.nix
    ../../service/ssh.nix
    # ../../service/antivirus_clamav.nix # too memory gready ; i have too many Out Of Memory with it
    ../../service/firewall.nix
    # ../../service/firewall_at_application_level_opensnitch.nix
    ../../service/kdeconnect.nix
    ../../service/virtualbox.nix
    ../../service/apparmor.nix
    ../../service/gpg_agent.nix
    ../../service/printer.nix
    ../../service/flatpak.nix
    ../../service/accessibility_screen_reader.nix
    ../../service/espanso.nix
    ../../service/ollama.nix
    ../../service/spotifyd.nix
    ../../package/gui/game.nix
    ../../fonts.nix
    ../../nix_flake.nix
    ../../../nix_cache/cache.nix
    ../../hardware/file_system/tmp.nix
    ../../service/nix_garbage_collector.nix
    ../../service/auto_upgrade.nix
    ../../service/nix_daemon.nix
    ../../service/firmware_update.nix
    ../../service/nix_trusted_users.nix
    ../../security/sudo_message.nix
    ../../xdg_portal.nix

    inputs.home-manager.nixosModules.home-manager
    ../../home-manager.nix
    {
      home-manager.users.pinage404 = {
        imports = [
          (../. + "./../../homes/x86_64-linux/pinage404@framework-16/default.nix")
        ];
      };
      home-manager.users.invited = {
        imports = [
          (../. + "./../../homes/x86_64-linux/invited@framework-16/default.nix")
        ];
      };
      home-manager.users.eb = {
        imports = [
          (../. + "./../../homes/x86_64-linux/eb@framework-16/default.nix")
        ];
      };

      home-manager.users.git = import ../../../systems/user/git/home.nix;
    }
  ];

  hardware.enableRedistributableFirmware = true;

  age.secrets."gitlab-runner/default".file = ../../../secrets/gitlab-runner/framework-16/default.age;
  age.secrets."gitlab-runner/default-2".file =
    ../../../secrets/gitlab-runner/framework-16/default-2.age;
  age.secrets."gitlab-runner/default-3".file =
    ../../../secrets/gitlab-runner/framework-16/default-3.age;
  age.secrets."gitlab-runner/nix".file = ../../../secrets/gitlab-runner/framework-16/nix.age;

  # https://github.com/NixOS/nix/issues/6536#issuecomment-1254858889
  age.secrets."nix/github/personnal_access_token" = {
    file = ../../../secrets/nix/github/personnal_access_token.age;
    mode = "0440";
    owner = "root";
    group = config.users.groups.keys.name;
  };
  nix = {
    extraOptions = ''
      !include ${config.age.secrets."nix/github/personnal_access_token".path}
    '';
  };

  # Copy the NixOS configuration file and link it from the resulting system (/run/current-system/configuration.nix)
  # This is useful in case you accidentally delete configuration.nix
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?
}
