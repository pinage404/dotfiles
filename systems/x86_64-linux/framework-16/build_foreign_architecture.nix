{
  boot.binfmt.emulatedSystems = [
    "armv6l-linux"
    "armv7l-linux"
    "aarch64-linux"
    # "i386-linux" # Steam put CPU at 100% and never start
    "i686-linux"
    "i686-windows"
    "x86_64-windows"
  ];
}
