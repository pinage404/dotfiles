{
  fileSystems."/media/game" = {
    device = "/dev/disk/by-label/root_btrfs";
    fsType = "btrfs";
    options = [
      "defaults"
      "subvol=media/game"
      "noauto" # do not mount on boot
      "x-systemd.automount" # mount when needed
      "x-systemd.idle-timeout=5m" # unmount after 5 min of inactivity
    ];
  };

  fileSystems."/media/game/steam" = {
    device = "/dev/disk/by-label/root_btrfs";
    fsType = "btrfs";
    options = [
      "defaults"
      "subvol=media/game/steam"
      "noauto" # do not mount on boot
      "x-systemd.automount" # mount when needed
      "x-systemd.idle-timeout=5m" # unmount after 5 min of inactivity
    ];
  };

  fileSystems."/media/game/heroic" = {
    device = "/dev/disk/by-label/root_btrfs";
    fsType = "btrfs";
    options = [
      "defaults"
      "subvol=media/game/heroic"
      "noauto" # do not mount on boot
      "x-systemd.automount" # mount when needed
      "x-systemd.idle-timeout=5m" # unmount after 5 min of inactivity
    ];
  };
}
