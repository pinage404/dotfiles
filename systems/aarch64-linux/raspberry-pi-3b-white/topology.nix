{
  inputs,
  ...
}:

{
  imports = [
    inputs.nix-topology.nixosModules.default
  ];

  topology.self = {
    deviceIcon = "devices.cloud-server";
    hardware.info = "Raspberry Pi 3";
    interfaces.eth0.network = "home";
    # hardware.image = ./docs/image/Raspberry_Pi_3_Model_B_V1.2_white_case.jpg; # take too much time https://github.com/oddlama/nix-topology/issues/6
  };
}
