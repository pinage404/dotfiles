{
  config,
  ...
}:

{
  _module.args.nixinate = {
    host = "${config.networking.hostName}.local";
    sshUser = "pi";
    buildOn = "local";
    substituteOnTarget = true;
    hermetic = false;
    nixOptions = [ ];
    dryRun = false;
  };
}
