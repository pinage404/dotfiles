{
  networking = {
    hostName = "raspberry-pi-3b-white";

    interfaces.eth0.ipv4.addresses = [
      {
        address = "192.168.1.101";
        prefixLength = 24;
      }
    ];
  };
}
