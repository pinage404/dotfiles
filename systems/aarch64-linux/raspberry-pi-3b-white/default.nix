# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’)

# test with
# nixos-rebuild build-vm -I nixos-config=./systems/aarch64-linux/raspberry-pi-3b-white/default.nix
# nixos-rebuild build-vm --flake ".#raspberry-pi-3b-white"
{
  inputs,
  ...
}:

{
  imports = [
    inputs.nixos-hardware.nixosModules.raspberry-pi-3
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./topology.nix
    ./nixinate.nix
    ./swapfile.nix
    ./networking.nix
    ../../networking/hosts.nix
    ../../networking/dns_domain_name_server.nix
    ../../networking/local.nix
    ../../hardware/server.nix
    ../../hardware/file_system/nix.nix
    ../../hardware/file_system/home.nix
    ../../hardware/file_system/tmp.nix
    ../../hardware/file_system/data_hdd.nix
    ../../nix_flake.nix
    ../../service/nix_garbage_collector.nix
    ../../service/auto_upgrade.nix
    ../../service/nix_daemon.nix
    ../../service/nix_trusted_users.nix
    ../../service/nix_ssh_serve.nix
    ../../service/ssh.nix
    #../../service/apparmor.nix # doesn't have compatible Kernel
    # ../../service/antivirus_clamav.nix
    ../../service/firewall.nix
    ../../service/power_management.nix
    # > The package works on aarch64 but uses PIO which uses FHSUserEnv and doesn't work on aarch64.
    # @colemickens:matrix.org
    # https://matrix.to/#/!ACOPNtJclKAfOLQCKK:matrix.org/$159952018644717kBaMj:matrix.org?via=matrix.org&via=kapsi.fi&via=kyju.org
    # ../../service/boinc.nix
    # ../../service/torrent_deluge.nix
    ../../service/nfs_network_file_system_server_media.nix
    ../../service/jellyfin_media_server.nix
    ../../service/systemd_journald_keep_free_space.nix
    ../../service/log_rotate.nix
    # ../../service/automatic-shutdown.nix # Raspberry PI shutdown just after booting ; probably it doesn't have battery to memorize time
    ../../user/pi.nix
    ../../package/cli/git.nix
    ../../package/cli/admin.nix
    ../../package/cli/tools.nix
    ../../package/cli/network.nix
    ../../../nix_cache/cache.nix
    ../../i18n.nix
    ../../time.nix
    ../../fonts.nix
  ];

  # This value determines the NixOS release from which the default settings for stateful data, like file locations and database versions on your system were taken
  # It's perfectly fine and recommended to leave this value at the release version of the first install of this system
  # Before changing this value read the documentation for this option (e.g. man configuration.nix or on https://nixos.org/nixos/options.html)
  system.stateVersion = "23.05"; # Did you read the comment?
}
