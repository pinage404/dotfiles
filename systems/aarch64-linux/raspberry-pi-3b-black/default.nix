# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page, on https://search.nixos.org/options and in the NixOS manual (`nixos-help`)

# test with
# nixos-rebuild build-vm -I nixos-config=./systems/aarch64-linux/raspberry-pi-3b-black/default.nix
# nixos-rebuild build-vm --flake ".#raspberry-pi-3b-black"
{
  inputs,
  ...
}:

{
  imports = [
    inputs.nixos-hardware.nixosModules.raspberry-pi-3
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./topology.nix
    ./nixinate.nix
    ./swapfile.nix
    ./networking.nix
    ../../networking/hosts.nix
    ../../networking/dns_domain_name_server.nix
    ../../networking/local.nix
    ../../hardware/server.nix
    ../../hardware/file_system/tmp.nix
    ../../nix_flake.nix
    ../../service/nix_garbage_collector.nix
    ../../service/auto_upgrade.nix
    ../../service/nix_daemon.nix
    ../../service/nix_trusted_users.nix
    ../../service/nix_ssh_serve.nix
    ../../service/ssh.nix
    #../../service/apparmor.nix # doesn't have compatible Kernel
    # ../../service/antivirus_clamav.nix
    ../../service/firewall.nix
    ../../service/adguard_home.nix
    ../../service/systemd_journald_keep_free_space.nix
    ../../service/log_rotate.nix
    # > The package works on aarch64 but uses PIO which uses FHSUserEnv and doesn't work on aarch64.
    # @colemickens:matrix.org
    # https://matrix.to/#/!ACOPNtJclKAfOLQCKK:matrix.org/$159952018644717kBaMj:matrix.org?via=matrix.org&via=kapsi.fi&via=kyju.org
    # ../../service/boinc.nix
    # ../../service/automatic-shutdown.nix # Raspberry PI shutdown just after booting ; probably it doesn't have battery to memorize time
    ../../user/pi.nix
    ../../package/cli/git.nix
    ../../package/cli/admin.nix
    ../../package/cli/tools.nix
    ../../package/cli/network.nix
    ../../package/cli/fish.nix
    ../../../nix_cache/cache.nix
    ../../i18n.nix
    ../../time.nix
    ../../fonts.nix
  ];

  # Copy the NixOS configuration file and link it from the resulting system (/run/current-system/configuration.nix)
  # This is useful in case you accidentally delete configuration.nix
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine, and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions
  #
  # Most users should NEVER change this value after the initial install, for any reason, even if you've upgraded your system to a new NixOS release
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from, so changing it will NOT upgrade your system
  #
  # This value being lower than the current NixOS release does NOT mean your system is out of date, out of support, or vulnerable
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration, and migrated your data accordingly
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion
  system.stateVersion = "23.11"; # Did you read the comment?
}
