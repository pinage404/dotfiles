{
  networking = {
    hostName = "raspberry-pi-3b-black";

    interfaces.eth0.ipv4.addresses = [
      {
        address = "192.168.1.100";
        prefixLength = 24;
      }
    ];
  };
}
