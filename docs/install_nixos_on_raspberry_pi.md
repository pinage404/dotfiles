# Install NixOS on Raspberry PI

- [1. Find your model](#1-find-your-model)
- [2. Find CPU type](#2-find-cpu-type)
- [3. Install on Raspberry PI](#3-install-on-raspberry-pi)

## 1. Find your model

[![Photo of different Raspberry Pi models](https://raspi.tv/wp-content/uploads/2018/12/Pi-Family-Photo-Master-Dec2018_1500.jpg)](https://raspi.tv/2018/new-raspberry-pi-family-photo-including-pi3a-plus-zero-wh)

[![FlowChart to determine Raspberry PI model](https://raspberrytips.com/wp-content/uploads/2020/04/raspberrypi-which-model-flowchart.jpg?ezimgfmt=ng:webp/ngcb29)](https://raspberrytips.com/which-raspberry-pi-model/)

https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/README.md

https://www.raspberrypi.org/documentation/hardware/raspberrypi/revision-codes/README.md

## 2. Find CPU type

[Look for the line `Instruction set` the table of Wikipedia](https://en.wikipedia.org/wiki/Raspberry_Pi#Specifications)

```sh
cat /proc/cpuinfo | grep Model
```

## 3. Install on Raspberry PI

For `Raspberry Pi 3 Model B V1.2` made in 2015, which is an `ARMv8-A (64/32-bit)` CPU

1. Follow [NixOS' Wiki to **get** the ARM **installer**](https://wiki.nixos.org/wiki/NixOS_on_ARM)
1. **Download** the last available image, [currently](https://hydra.nixos.org/build/130467108)
1. **Uncompress** image

   ```sh
   # nix-shell --packages zstd --run "unzstd nixos-sd-image-<nixos-channel>.<build>-aarch64-linux.img.zst"
   # in my case
   nix-shell --packages zstd --run "unzstd nixos-sd-image-20.09.1818.98e46cec92f-aarch64-linux.img.zst"
   ```

All **commands below** are executed with **root** privileges

```sh
su
```

1. **Check** your devices

   ```sh
   fdisk -l
   ```

1. **Plug** SD card
1. **Find** the new **device** by comparing with the previous output

   ```sh
   fdisk -l
   ```

1. **Copy image** on the SD card

   ```sh
   # dd status=progress if=nixos-sd-image-<nixos-channel>.<build>-aarch64-linux.img of=/dev/<device>
   # in my case
   dd status=progress if=nixos-sd-image-20.09.1818.98e46cec92f-aarch64-linux.img of=/dev/sdc
   ```

1. **Plug** SD card **into** the **Raspberry Pi**
1. **Plug-in ethernet** cable
1. **Plug-in HDMI**

   and hope to not be stuck with [the "Starting Kernel..." issue](https://github.com/NixOS/nixpkgs/issues/104609)

1. **Plug-in power**
1. **Set a password** for `nixos` user

   ```sh
   passwd
   ```

   default user is `nixos` and default password is `nixos`

1. **Find** your **IP address**

   ```sh
   ip addr show eth0 | grep "inet " # on old NixOS
   ip addr show enu1u1 | grep "inet " # on new NixOS
   ```

1. **Connect** to your Raspberry Pi

   if you have many (6 or more, by default) SSH keys

   SSH may try to connect with every keys

   there is no SSH keys pushed on the machine yet

   there is no need to try to connect with a SSH key

   force the option `-o IdentitiesOnly=yes` in order to prevent this behavior

   ```sh
   # ssh -o IdentitiesOnly=yes nixos@<your-ip-address>
   # in my case
   ssh -o IdentitiesOnly=yes nixos@192.168.1.23
   ```

1. **Set `root`'s password**

   ```sh
   sudo su
   passwd
   ```

1. [**Add a swap file**](https://linuxize.com/post/create-a-linux-swap-file/#how-to-add-swap-file)

   On Raspberry Pi, RAM is often not enougth to build NixOS

   To add more memory, manually [add a swap file](https://linuxize.com/post/create-a-linux-swap-file/#how-to-add-swap-file) for the first installation

   ```sh
   SWAPFILE_PATH="/swapfile"
   SWAPFILE_SIZE="512M"

   sudo fallocate --length "$SWAPFILE_SIZE" "$SWAPFILE_PATH"
   sudo chmod 600 "$SWAPFILE_PATH"
   sudo mkswap "$SWAPFILE_PATH"
   sudo swapon "$SWAPFILE_PATH"
   ```

   And [remove it](https://linuxize.com/post/create-a-linux-swap-file/#how-to-remove-swap-file) after the installation

   ```sh
   SWAPFILE_PATH="/swapfile"

   sudo swapoff "$SWAPFILE_PATH"
   sudo rm "$SWAPFILE_PATH"
   ```

Follow [installation instructions](../maskfile.md#install)

Inspired by [this tutorial](https://citizen428.net/blog/installing-nixos-raspberry-pi-3)
