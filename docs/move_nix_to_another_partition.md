# Move `/nix` to another partition

[Mounting `/nix` on its own partition](https://gitlab.com/pinage404/dotfiles/-/blob/7df8ccba89591baf29793367c1e463503a21b7a7/nixos/hardware/file_system/nix.nix) allows it to [manage the free space itself](https://gitlab.com/pinage404/dotfiles/-/blob/7df8ccba89591baf29793367c1e463503a21b7a7/nixos/service/nix_garbage_collector.nix#L2) without wasting space on the rest of the system

All commands below are executed with root privileges

1. Create a new partition
1. Move `/nix` to another partition

   ```sh
   NIX_CONFIG='extra-experimental-features = flakes nix-command' \
   nix run gitlab:pinage404/dotfiles#move_nix_to_another_partition
   ```

   or

   ```sh
   NIX_CONFIG='extra-experimental-features = flakes nix-command' \
   nix run gitlab:pinage404/dotfiles#move_nix_to_another_partition /dev/disk/by-label/nix
   ```

1. Add [mounting the `/nix` partition](/systems/hardware/file_system/nix.nix) to your `/etc/nixos/configuration.nix`

   ```nix
   {
     # ...
     fileSystems."/nix" = {
       device = "/dev/disk/by-label/nix";
       fsType = "ext4";
       neededForBoot = true;
       options = [ "noatime" ];
     };
     # ...
   }
   ```

1. Apply your configuration

   ```sh
   mask nixos switch
   ```

1. Reboot to be sure `/nix/store` is properly mounted
1. After reboot, check that `/nix` is mounted over your partition

   ```sh
   mount | grep '/nix' && echo "Nix seems to use your new partition" || echo "It seems that something bad happened"
   ```

1. Once **you are sure** everything works, you can delete the old store

   ```sh
   mkdir /tmp/old_root
   mount --bind / /tmp/old_root
   rm --recursive /tmp/old_root/nix
   umount /tmp/old_root
   rmdir /tmp/old_root
   ```

From [the wiki : Move `/nix` to another partition](https://nixos.wiki/wiki/Storage_optimization#Moving_the_store)
