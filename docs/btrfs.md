# BTRFS

As root

```sh
lsblk
```

```txt
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
loop0    7:0    0   2.4G  1 loop /nix/.ro-store
sda      8:0    0 119.2G  0 disk 
sdb      8:16   1  29.8G  0 disk 
├─sdb1   8:17   1   2.5G  0 part /iso
└─sdb2   8:18   1     3M  0 part 
sr0     11:0    1   128M  0 rom  
```

## Partitionning

```sh
fdisk /dev/sda
```

### Create partition table

`g` create a new empty GPT partition table

### Create boot partition

`n` add a new partition

Last sector `+512M`

`t` change a partition type

Partition type or alias (type L to list all): `uefi`

### Create BTRFS partition

`n` add a new partition

`t` change a partition type

Partition type or alias (type L to list all): `linux`

### Print partition table

`p` print the partition table

```txt
Disk /dev/sda: 119.24 GiB, 128035676160 bytes, 250069680 sectors
Disk model: MTFDDAK128MBF-1A
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: C1905025-9D20-4A96-8FC3-B5324FC97C8E

Device       Start       End   Sectors   Size Type
/dev/sda1     2048   1050623   1048576   512M EFI System
/dev/sda2  1050624 250068991 249018368 118.7G Linux filesystem

Filesystem/RAID signature on partition 1 will be wiped.
Filesystem/RAID signature on partition 2 will be wiped.
```

### Write partition table

`w` write table to disk and exit

```txt
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

## Formatting

### Format boot partition

```sh
mkfs.vfat -F 32 -n BOOT /dev/sda1
```

### Format BTRFS partition

```sh
mkfs.btrfs --label root_btrfs /dev/sda2
```

## Mounting

```sh
mount /dev/sda2 /mnt/
```

### Create sub volumes

#### Create home sub volume

```sh
btrfs subvolume create /mnt/home
```

#### Mount home sub volume

```sh
mount /dev/sda2 /mnt/home -o subvol=home
```

#### Create nix sub volume

```sh
btrfs subvolume create /mnt/nix
```

#### Mount nix sub volume

```sh
mount /dev/sda2 /mnt/nix -o subvol=nix
```

#### Create nix store sub volume

```sh
btrfs subvolume create /mnt/nix/store
```

#### Mount nix store sub volume

```sh
mount /dev/sda2 /mnt/nix/store -o subvol=nix/store
```

### Mount boot partition

```sh
mkdir /mnt/boot
```

```sh
mount /dev/sda1 /mnt/boot/
```

## Install NixOS

```sh
nixos-generate-config --root /mnt/
```

```sh
nano /mnt/etc/nixos/configuration.nix
```

* set boot loader
* set keyboard layout
* enable SSH

```nix
{
  # ...
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  console = {
    keyMap = "fr";
  };
  services.xserver.xkb.layout = "fr";
  services.openssh.enable = true;
}
```

```sh
lsblk
```

```txt
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda      8:0    0 119,2G  0 disk
├─sda1   8:1    0   512M  0 part /boot
└─sda2   8:2    0 118,7G  0 part /home
                                 /nix/store
                                 /nix/store
                                 /nix
                                 /
sr0     11:0    1   128M  0 rom
```

```sh
nixos-install
```
