# Packages

There are [some packages](../packages/) that i have manually packaged with [Nix](https://nixos.org/)

You can submit them to [nixpkgs](https://github.com/NixOS/nixpkgs), please [let me know](https://gitlab.com/pinage404/dotfiles/-/issues)

[Some packages have been upstreamed](https://github.com/NixOS/nixpkgs/pulls?q=is%3Apr+author%3Apinage404) and are no longer repackaged in this repository

## Try without installing

You can try some packages without installing it on your system by running the following command

```sh
nix run gitlab:pinage404/dotfiles#git-foreach
```

Some packages need to be installed then reboot to work, so this method may not work

## Using packages

While they are not available on [nixpkgs](https://github.com/NixOS/nixpkgs) you can use them like this

Read [`flake.nix`](./using_packages/flake.nix) [^minimal_setup] and follow the `# add` comments

[^minimal_setup]: This is a minimal working setup just for the demo
