{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";

    pinage404-dotfiles.url = "gitlab:pinage404/dotfiles"; # add inputs
  };

  outputs =
    {
      nixpkgs,

      pinage404-dotfiles, # add argument

      ...
    }:
    {
      nixosConfigurations.your_hostname = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          (
            { pkgs, ... }:
            {
              boot.loader.systemd-boot.enable = true;
              fileSystems."/" = {
                device = "/dev/disk/by-uuid/533d1106-d63b-4217-94cc-0ae996349392";
                fsType = "btrfs";
              };
              system.stateVersion = "23.11";
              users.users.your_user = {
                isNormalUser = true;
              };

              nixpkgs.overlays = [ pinage404-dotfiles.overlays.default ]; # add overlay to nixpkgs
              environment.systemPackages = [
                pkgs.internal.vscode-runner # add package with flake and with overlay see above
                # or
                pinage404-dotfiles.packages."x86_64-linux".vscode-runner # add package with flake but without overlay
                # or
                (builtins.getFlake "gitlab:pinage404/dotfiles/4a30c8d026366d8a0109ccecfef19cd46a72912e")
                .packages.x86_64-linux.vscode-runner # add package without overlay and without flake
              ];
            }
          )
        ];
      };
    };
}
