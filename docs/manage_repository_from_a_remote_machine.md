# Manage repository from a remote machine

1. Clone the repository on `brain machine` and `zombie machine`

   ```sh
   git clone https://gitlab.com/pinage404/dotfiles.git
   cd dotfiles
   ```

1. On `brain machine`, add a remote to `zombie machine`

   Assuming that repository is named `dotfiles` and directly in the `zombie_user`'s home directory

   ```sh
   git remote add zombie zombie_user@zombie_machine.local:dotfiles
   ```

1. On `zombie machine`, _dangerously_ enable overwriting working tree

   **Note :** it could be _dangerous_, be sure to restrict access to your machine before doing this

   ```sh
   git config receive.denyCurrentBranch updateInstead
   ```

1. On `brain machine`, update your configuration

   ```sh
   nano systems/service/automatic-shutdown.nix
   git commit --patch
   ```

1. On `brain machine`, send it to `zombie machine`

   - Do it with the super tool : [`pushnix`](https://github.com/arnarg/pushnix)

      This will

      1. `git push` from `brain machine` to `zombie machine`
      1. `nixos-rebuild switch` on `zombie machine`

      ```sh
      pushnix deploy zombie
      ```

   - With hooks and some config

      On `zombie machine`, configure git to set hooks path

      ```sh
      git config core.hooksPath $PWD/hooks
      ```

      **Note :** It's needed to do it once per machine

      Then, when you want to push and apply the changes in the configuration from the `brain machine`

      ```sh
      git push zombie
      ```

   - Without `pushnix` nor hooks

      On `brain machine`

      ```sh
      git push zombie
      ```

      On `zombie machine`

      ```sh
      sudo nixos-rebuild switch
      ```
