{
  description = "Home Manager & NixOS & Nix Darwin & Nix on Droid configuration";

  inputs = {
    snowfall-lib = {
      url = "github:snowfallorg/lib";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-compat.follows = "flake-compat";
      inputs.flake-utils-plus.follows = "flake-utils-plus";
    };

    # system targets

    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";

    nixpkgs-old.url = "github:nixos/nixpkgs/nixos-24.05";

    nixpkgs-current.url = "github:nixos/nixpkgs/nixos-24.11";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs-current";
    };

    nixpkgs-darwin.url = "github:nixos/nixpkgs/nixpkgs-24.11-darwin";
    darwin = {
      url = "github:lnl7/nix-darwin/nix-darwin-24.11";
      inputs.nixpkgs.follows = "nixpkgs-darwin";
    };

    nix-on-droid = {
      url = "github:nix-community/nix-on-droid/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs-old";
      inputs.home-manager.follows = "home-manager";
      inputs.nixpkgs-docs.follows = "";
      inputs.nmd.follows = "";
      inputs.nix-formatter-pack.follows = "";
    };

    # configuration and tools

    nixpkgs-lib.url = "github:nix-community/nixpkgs.lib";

    # determinate.url = "https://flakehub.com/f/DeterminateSystems/determinate/0.1";

    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    nixos-hardware.url = "github:NixOS/nixos-hardware";

    brew-nix = {
      url = "github:BatteredBunny/brew-nix";
      inputs.brew-api.follows = "brew-api";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nix-darwin.follows = "darwin";
    };
    # update packages available in brew
    # without the needed to wait another release from brew-nix
    brew-api = {
      url = "github:BatteredBunny/brew-api";
      flake = false;
    };

    nixcasks = {
      url = "github:jacekszymanski/nixcasks";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    mac-app-util = {
      url = "github:hraban/mac-app-util";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-compat.follows = "flake-compat";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.darwin.follows = "darwin";
      inputs.home-manager.follows = "home-manager";
      inputs.systems.follows = "systems";
    };

    nixinate = {
      url = "github:matthewcroughan/nixinate";
      inputs.nixpkgs.follows = "nixpkgs-current";
    };

    nix-topology = {
      url = "github:oddlama/nix-topology";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-utils.follows = "flake-utils";
      inputs.pre-commit-hooks.follows = "";
      inputs.devshell.follows = "";
    };

    nix-software-center = {
      url = "github:snowfallorg/nix-software-center";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.utils.follows = "flake-utils";
    };

    nixos-conf-editor = {
      url = "github:snowfallorg/nixos-conf-editor";
      inputs.snowfall-lib.follows = "snowfall-lib";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-compat.follows = "flake-compat";
    };

    snowfall-frost = {
      url = "github:snowfallorg/frost";
      inputs.snowfall-lib.follows = "snowfall-lib";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.unstable.follows = "nixpkgs-unstable";
      inputs.nmd.follows = "nmd";
    };

    nix-alien = {
      url = "github:thiagokokada/nix-alien";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-compat.follows = "";
    };

    nix-snapshotter = {
      url = "github:pdtpartners/nix-snapshotter";
      inputs.nixpkgs.follows = "nixpkgs-current";
      inputs.flake-compat.follows = "";
    };

    zen-browser = {
      url = "github:0xc000022070/zen-browser-flake";
      inputs.nixpkgs.follows = "nixpkgs-current";
    };

    # repositories packaged by this configuration see ./docs/packages.md

    tcr = {
      url = "github:murex/TCR/v1.2.0";
      flake = false;
    };

    git-split = {
      url = "github:d-e-s-o/git-split";
      flake = false;
    };

    git-foreach = {
      url = "github:d-e-s-o/git-foreach/devel";
      flake = false;
    };

    krunner-spotify = {
      url = "github:MartijnVogelaar/krunner-spotify";
      flake = false;
    };

    pushnix = {
      url = "github:arnarg/pushnix";
      flake = false;
    };

    # deduplicate indirect dependencies

    systems.url = "github:nix-systems/default";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };

    flake-utils-plus = {
      url = "github:gytis-ivaskevicius/flake-utils-plus";
      inputs.flake-utils.follows = "flake-utils";
    };

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    nmd = {
      url = "gitlab:rycee/nmd";
      inputs.nixpkgs.follows = "nixpkgs-current";
    };
  };

  outputs =
    inputs@{
      self,
      nix-on-droid,
      ...
    }:
    # uncomment to debug with `traceVal someValue` `traceSeq someValue` `traceValSeq someValue` `traceValSeqN 2 someValue`
    # with import ./systems/nix/debug.nix { inherit nixpkgs; };
    let
      inherit (inputs.nixpkgs-lib) lib;

      osVersion = "sonoma";

      nixcasksOverlay = _final: prev: {
        nixcasks =
          (inputs.nixcasks.output {
            inherit osVersion;
          }).packages."${prev.system}";
      };

      externalOverlays = [
        inputs.nix-topology.overlays.default
        inputs.nix-software-center.overlays.pkgs
        inputs.darwin.overlays.default
        inputs.snowfall-frost.overlays.default
        inputs.agenix.overlays.default
        inputs.brew-nix.overlays.default
        nixcasksOverlay
        inputs.nix-snapshotter.overlays.default
      ];

      makeNixinateApp = lib.attrsets.mapAttrs' (
        name: value: lib.attrsets.nameValuePair ("deploy-" + name) value
      );
    in
    (inputs.snowfall-lib.mkFlake {
      inherit inputs;
      src = ./.;
      overlays = externalOverlays;
      systems.modules.nixos = [
        inputs.agenix.nixosModules.default
        # inputs.determinate.nixosModules.default
        inputs.nix-snapshotter.nixosModules.default
      ];
      systems.modules.darwin = [
        inputs.agenix.darwinModules.default
        # inputs.determinate.darwinModules.default
        inputs.mac-app-util.darwinModules.default
      ];
      homes.modules = [
        inputs.agenix.homeManagerModules.default
        inputs.mac-app-util.homeManagerModules.default
      ];
      alias.shells.default = "direnv_and_vscode";
      outputs-builder = channels: {
        formatter = channels.nixpkgs-current.nixfmt-rfc-style;

        apps =
          makeNixinateApp
            (inputs.nixinate.nixinate."${channels.nixpkgs-current.system}" self).nixinate;
      };
    })
    // {
      nixOnDroidConfigurations."fairphone_4" = nix-on-droid.lib.nixOnDroidConfiguration {
        pkgs = import inputs.nixpkgs-current {
          system = "aarch64-linux";

          overlays =
            let
              internalOverlays = builtins.attrValues self.overlays;
            in
            externalOverlays ++ internalOverlays;
        };
        modules = [
          {
            _module.args = {
              inherit inputs;

              system = "aarch64-linux";
            };
          }
          ./systems/machine/fairphone_4/nix-on-droid.nix
        ];
      };
    };
}
