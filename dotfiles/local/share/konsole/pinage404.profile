[General]
Command=fish
Name=pinage404
Parent=FALLBACK/

[Interaction Options]
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
