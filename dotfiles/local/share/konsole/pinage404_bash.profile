[General]
Command=bash
Name=pinage404_bash
Parent=FALLBACK/

[Interaction Options]
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
