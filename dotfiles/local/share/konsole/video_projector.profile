[Appearance]
ColorScheme=BlackOnWhite
Font=FiraCode Nerd Font Mono,20,-1,5,50,0,0,0,0,0

[General]
Command=fish
Name=video_projector
Parent=FALLBACK/

[Interaction Options]
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
