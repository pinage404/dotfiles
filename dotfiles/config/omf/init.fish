set -g default_user pinage404

# directory length
set -g fish_prompt_pwd_dir_length 1
set -g theme_project_dir_length 0

set -g theme_newline_cursor yes

# color
function bobthefish_colors -S -d 'Define a custom bobthefish color scheme'
	# optionally include a base color scheme...
	__bobthefish_colors dark

	# then override everything you want! note that these must be defined with `set -x`
	# set -x color_initial_segment_exit ffffff ce000f --bold
	# set -x color_initial_segment_su ffffff 189303 --bold
	# set -x color_initial_segment_jobs ffffff 255e87 --bold
	# set -x color_path 333333 999999
	# set -x color_path_basename 333333 ffffff --bold
	# set -x color_path_nowrite 660000 cc9999
	# set -x color_path_nowrite_basename 660000 cc9999 --bold
	# set -x color_repo addc10 0c4801
	# set -x color_repo_work_tree 333333 ffffff --bold
	# set -x color_repo_dirty ce000f ffffff
	# set -x color_repo_staged f6b117 3a2a03
	# set -x color_vi_mode_default 999999 333333 --bold
	set -x color_vi_mode_default 999999 3a2a03 --bold
	# set -x color_vi_mode_insert 189303 333333 --bold
	set -x color_vi_mode_insert 189303 3a2a03 --bold
	# set -x color_vi_mode_visual f6b117 3a2a03 --bold
	# set -x color_vagrant 48b4fb ffffff --bold
	# set -x color_username cccccc 255e87 --bold
	set -x color_username 555555 255e87 --bold
	# set -x color_hostname cccccc 255e87
	set -x color_hostname 555555 255e87
	# set -x color_rvm af0000 cccccc --bold
	# set -x color_virtualfish 005faf cccccc --bold
	# set -x color_virtualgo 005faf cccccc --bold
	# set -x color_desk 005faf cccccc --bold
end

# font
set -g theme_avoid_ambiguous_glyphs yes
set -g theme_nerd_fonts yes
set -g theme_powerline_fonts yes

# datetime
set -g theme_display_date yes
set -g theme_date_format "+%H:%M:%S"
set -g theme_display_cmd_duration yes

# exit status
set -g theme_show_exit_status yes

# control version system
set -g theme_display_git yes
set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_untracked yes
set -g theme_display_git_dirty yes
set -g theme_display_git_dirty_verbose yes
set -g theme_display_git_master_branch yes
set -g theme_display_git_stashed_verbose yes
# set -g theme_git_worktree_support yes
set -g theme_display_hg yes

# environment
set -g theme_display_docker_machine yes
set -g theme_display_k8s_context yes
set -g theme_display_nvm yes
set -g theme_display_ruby yes
set -g theme_display_vagrant yes
set -g theme_display_vi yes
set -g theme_display_virtualenv yes
set -g theme_display_user ssh
set -g theme_display_hostname ssh

# title
set -g theme_title_display_process yes
set -g theme_title_display_path yes
set -g theme_title_use_abbreviated_path no
set -g theme_title_display_user yes
