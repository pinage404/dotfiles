function serve --description "Serve files with a simple HTTP server"
    nix-shell --packages python3 --command "python -m http.server $argv"
end

