function run-nix --description "Run package in a nix-shell"
	nix-shell --command fish --packages $argv
end
