# copied from https://github.com/nanoxd/nanofiles/blob/master/config/fish/functions/mkcd.fish

function mkcd --description "Make directory and cd into it"
	mkdir $argv
	and cd $argv
end
