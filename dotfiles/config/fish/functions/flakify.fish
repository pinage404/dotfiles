function flakify --description 'Shell function to quickly setup nix flake + direnv in a new project'
    if test ! -e flake.nix
        nix flake init --template github:nix-community/nix-direnv
        direnv allow
    else if test ! -e ./.envrc
        echo "use flake" >.envrc
        direnv allow
    end
    eval $EDITOR flake.nix
end
