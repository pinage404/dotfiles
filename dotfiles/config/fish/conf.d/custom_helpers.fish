alias run="podman run --rm --interactive --tty --volume (pwd):/w --workdir /w --init"
alias run-gui="xhost local:root ; run --device /dev/snd --volume /tmp/.X11-unix:/tmp/.X11-unix:rw --env DISPLAY=$DISPLAY"

if status is-interactive
    abbr --add --global nix-sandboxes "nix run gitlab:pinage404/nix-sandboxes --"
    abbr --add --global nix-sandboxes-list "nix run gitlab:pinage404/nix-sandboxes#sandboxes-list"
end
