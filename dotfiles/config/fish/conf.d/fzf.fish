set -gx FZF_DEFAULT_COMMAND "fd --type file --follow --color=always"
set -gx FZF_DEFAULT_OPTS --ansi

# search file / folder
set -gx FZF_CTRL_T_COMMAND "fd --type f --type d --hidden --follow --exclude .git"
set -gx FZF_CTRL_T_OPTS "
	--preview '
		([[ -d {} ]] && lsd --tree --color=always {}) ||
		([[ \$(file --mime {}) =~ binary ]] && hexyl {}) ||
		(bat --style=numbers --color=always {} ||
		cat {}) 2> /dev/null | head -50
	'
"

# command history
set -gx FZF_CTRL_R_OPTS "
	--preview 'echo {}'
	--preview-window down:3:hidden:wrap
	--bind '?:toggle-preview'
	--bind 'ctrl-y:execute-silent(echo -n {2..} | fish_clipboard_copy)+abort'
	--header 'Press CTRL-Y to copy command into clipboard'
	--border
"

# change directory
set -gx FZF_ALT_C_COMMAND "fd --type d --hidden --follow --exclude .git"
set -gx FZF_ALT_C_OPTS "--preview 'lsd --tree --color=always {} | head -50'"

if status --is-interactive
    bind \cp fzf-file-widget
    bind \ch fzf-history-widget
    # bind \ec fzf-cd-widget

    if bind -M insert >/dev/null 2>&1
        bind -M insert \cp fzf-file-widget
        bind -M insert \ch fzf-history-widget
        # bind -M insert \ec fzf-cd-widget
    end
end
