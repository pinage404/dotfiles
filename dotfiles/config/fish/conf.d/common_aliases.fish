if status is-interactive
    abbr --add --global ll "lsd -l --header --git"
    abbr --add --global la "lsd -a --header --git"
    abbr --add --global lt "lsd --tree"
    abbr --add --global lla "lsd -la--header --git"
end
