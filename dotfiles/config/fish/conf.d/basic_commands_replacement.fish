if status is-interactive
    alias cat="bat"
    alias tree="lsd --tree"
    # alias find="fd"
    alias grep="rg"

    abbr --add --global ls lsd
end
