set -gx BROWSER zen
set -gx EDITOR nano
set -gx VISUAL "code --wait"
set -gx MANPAGER "sh -c 'col -bx | bat --language man --plain'"
set -gx MANROFFOPT "-c"
