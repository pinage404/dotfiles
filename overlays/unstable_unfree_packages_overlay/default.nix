{
  inputs,
  ...
}:

_final: pkgs: {
  unstable_unfree = import inputs.nixpkgs-unstable {
    inherit (pkgs) system;
    config = {
      allowUnfree = true;
    };
  };
}
