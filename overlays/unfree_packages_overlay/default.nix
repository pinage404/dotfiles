{
  inputs,
  ...
}:

_final: pkgs:
let
  unfreePkgs = import inputs.nixpkgs-current {
    inherit (pkgs) system;
    config = {
      allowUnfree = true;
    };
  };
in
{
  unfree = unfreePkgs;

  inherit (unfreePkgs)
    steam
    steamPackages
    steam-unwrapped
    ;
}
