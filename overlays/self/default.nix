{
  inputs,
  ...
}:

_final: pkgs: {
  lib = pkgs.lib // {
    inherit (inputs) self;
  };
}
