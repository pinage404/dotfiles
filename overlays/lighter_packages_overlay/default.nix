{
  inputs,
  ...
}:

_final: prev:
let
  pkgs = inputs.nixpkgs-current.legacyPackages."${prev.system}";
in
{
  tesseract3 = pkgs.tesseract3.override {
    enableLanguages = [
      "eng"
      "fra"
    ];
  };
}
