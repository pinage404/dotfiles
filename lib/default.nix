{
  lib,
  ...
}:

{
  get_user_home =
    pkgs: name:
    (
      if pkgs.stdenv.hostPlatform.isDarwin then
        lib.mkDefault "/Users/${name}"
      else if pkgs.stdenv.hostPlatform.isLinux then
        lib.mkDefault "/home/${name}"
      else
        null
    );

  make_main_normal_user_extra_groups =
    config:

    [
      "fuse" # to be able to use FUSE to use AppImage
    ]
    ++ (lib.optional config.hardware.pulseaudio.systemWide "audio") # to be able to play sound with PulseAudio
    ++ (lib.optional config.services.pipewire.systemWide "pipewire") # to be able to play sound with Pipewire
    ++ (lib.optional config.services.lirc.enable "lirc") # to be able to use infrared remote controller with Lirc
    ++ (lib.optional config.networking.networkmanager.enable "networkmanager")
    ++ (lib.optional config.virtualisation.docker.enable "docker")
    ++ (lib.optional config.virtualisation.podman.dockerSocket.enable "podman")
    ++ (lib.optional config.virtualisation.virtualbox.host.enable "vboxusers") # to be able to use USB in VM
  ;
}
