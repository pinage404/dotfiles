{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [ inputs.self.devShells."${system}".direnv ];
  packages = [
    inputs.nix-on-droid.packages."${system}".nix-on-droid
  ];
}
