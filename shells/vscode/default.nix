{
  pkgs,
  ...
}:

pkgs.mkShellNoCC {
  packages = [
    pkgs.nixd # LSP
    pkgs.shfmt
  ];
}
