{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [ inputs.self.devShells."${system}".direnv ];
  packages = [
    pkgs.darwin-rebuild
    pkgs.nvd
  ];
}
