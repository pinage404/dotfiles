{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [ inputs.self.devShells."${system}".direnv ];
  packages = [
    pkgs.python3Minimal
  ];
}
