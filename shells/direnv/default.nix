{
  pkgs,
  ...
}:

pkgs.mkShellNoCC {
  packages = [
    pkgs.gitMinimal
    pkgs.mask
    pkgs.direnv
    pkgs.procps
    pkgs.toybox

    pkgs.hostname
    pkgs.coreutils
    pkgs.gnused
  ];
}
