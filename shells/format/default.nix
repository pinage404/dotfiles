{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [ inputs.self.devShells."${system}".direnv ];
  packages = [
    pkgs.shellcheck
    pkgs.nixfmt-rfc-style
    pkgs.diffutils
    pkgs.fd
    pkgs.gitMinimal
  ];
}
