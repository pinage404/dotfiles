{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [ inputs.self.devShells."${system}".direnv ];
  packages = [
    pkgs.deadnix
    pkgs.statix
    pkgs.fd
    pkgs.masklint
    pkgs.shellcheck
    pkgs.nixpkgs-lint-community
  ];
}
