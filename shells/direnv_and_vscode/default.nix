{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [
    inputs.self.devShells."${system}".direnv
    inputs.self.devShells."${system}".vscode
  ];
}
