{
  pkgs,
  inputs,
  system,
  ...
}:

pkgs.mkShellNoCC {
  inputsFrom = [ inputs.self.devShells."${system}".direnv ];
  packages =
    pkgs.lib.optionals pkgs.stdenv.hostPlatform.isLinux [
      pkgs.nixos-rebuild
    ]
    ++ [
      pkgs.nvd
      pkgs.coreutils # ensure to have the good `tail` on Mac
    ];
}
