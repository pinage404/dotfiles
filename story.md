# Story

I have tried many Linux distributions (Ubuntu, Linux Mint, Debian, Linux Mint Debian Edition, OpenSUSE, Chakra, SolydXK, Raspbian, CentOS and much more… before NixOS)

## Dotfiles Begin

In 2016, i wanted a solution to keep my configuration files, so i created [my dotfiles repository](https://gitlab.com/pinage404/dotfiles/-/blob/5cc45ca51811878f3a200a69679394a3d5408a9c/.gitconfig)

## Scripting

Soon, i started to have [scripts that install everything](https://gitlab.com/pinage404/dotfiles/-/blob/4084fd132658bce1cf01ff694e1173d097b0cacf/install/cli/install_from_basic_repo.sh) when i use a new distributions

But the scripts didn't work well depending on the distributions (`apt` VS `rpm`, packages named differently depending on the repository, packages not available …)

## Containerisation

Then, in 2017, i discovered the reproductability of Docker

In 2018, i tried to put [everything in a container](https://gitlab.com/pinage404/work-toolbox)… i failed, it was not really usuable

## DirEnv + Nix = ❤️

Later, still in 2018, [a friend](https://masto.ai/@xdetant) showed me [a magical setup that installs everything needed when entering in a folder](https://github.com/FaustXVI/sandboxes/blob/b0f69e9b17451d8ab09450617dde0c3a9a3ec757/rust/default.nix) (with [DirEnv](https://direnv.net/) + [Nix](https://nixos.org))

I thought this was done by the distribution, so i installed [NixOS 17.09](https://gitlab.com/pinage404/dotfiles/-/blob/cd3d5009a7ff2b1c258a3877d00f71b4fd75e2b9/configuration.nix), and struggled for weeks to get a minimum viable distribution

It was hard to get started, but the reproducibility, versioning of the system and ease of rollback make it so i have never gone back

## Home Manager

Then i used [Home Manager](https://gitlab.com/pinage404/dotfiles/-/blob/0d06eb13b846dbb84d14b4e735956990fd0c633b/.config/nixpkgs/home.nix) to handle packages separatly from the system

## Accross Operating Systems

A few years later, i used this Home Manager configuration with Ubuntu at work, then with Mac OS X at work (where i have no choice of operating system)

## Nix Darwin

A couple of years later, i started using [Nix Darwin](https://gitlab.com/pinage404/dotfiles/-/blob/c3074a7cccedfa061918d0ad2949596bea287bee/nixos/machine/mac_os_x/darwin-configuration.nix) to better handle the Mac OS X configuration (i still dislike Mac OS X even with Nix Darwin)

## Snowfall

Since 2024, i started using [Snowfall/lib](https://snowfall.org/guides/lib/quickstart) in order to [use the folders structure to generate common Nix configurations](https://gitlab.com/pinage404/dotfiles/-/commit/556b9707a99a97c710fc2dfbba65aa3cdc2705bc#58cb4f58166586c1ed7f076c568d41682df3661c_251_232)

## Initiations

Since then, i have introduced a few colleagues and friends to [DirEnv and Nix](https://pinage404.gitlab.io/slides/reproductible_dev_env/)

## Disliking Nix Language

I have never liked the Nix language : the dynamic typing system prevents me from understanding and knowing what i am using :

* Is it an array, an attribute set, a function ?
* What are the arguments of this function ?
* Which keys are needed ?
* What optional keys can i give ?
* What options can i use ?
* How do i determine which version i am currently using ?
* …

Everything is open source, but it can be very long and difficult to read the code :
for example,
when using [a function that takes an attribute set](https://github.com/NixOS/nixpkgs/blob/498aefe70f24b1886922e39e4e197889cc1bc493/pkgs/build-support/mkshell/default.nix#L15),
takes a few values
and [passes the attribute set to another function](https://github.com/NixOS/nixpkgs/blob/498aefe70f24b1886922e39e4e197889cc1bc493/pkgs/build-support/mkshell/default.nix#L58)
and [then starts all over again](https://github.com/NixOS/nixpkgs/blob/030755bf547ebb7ccebb360b3c2d293e26043a50/pkgs/stdenv/generic/make-derivation.nix#L166)

## DevBox

There are tools for abstracting the Nix language into another language

Sometimes in a well-known language ; most of the time, in a niche language, or a custom language created mainly for this use case

I have not mastered the Nix language yet,
i mainly dislike these tools because they add a layer of abstraction on top of Nix,
when it works, it's cool, it's auto-magic : it's simpler / more powerful, it works ;
but when it does not work or when i need to do something that is not supported by the tool, i am stuck with my lack of understanding of the lower layers

In 2023, a friend tells me that [DevBox](https://www.jetify.com/devbox/) is easy to use

I had seen it before but had not paid attention because i dislike auto-magic tools

I gave it a try :

* the documentation was good
* setup was super easy
* it does not reinvent the wheel
* it does not plug in at a super low level
* it simply generates Nix Flakes files and does smart things for specific tools with plugins

|                                                    | DevBox            | Nix                          |
|:---------------------------------------------------|:------------------|:-----------------------------|
| environment as code                                | ✅                 | ✅                            |
| freeze dependencies precisely                      | ✅                 | ✅                            |
| target a specific version (`nodejs@20.2`)          | ✅ easy            | ❌ hard                       |
| learn new language                                 | ✅ JSON : easy     | ❌ Nix : hard -> easy -> hard |
| similar to others tools (`npm`, `yarn`, `cargo` …) | ✅                 | ❌ hard                       |
| configuration as code                              | ❌ not in the tool | ✅                            |
| advanced use case                                  | ❌ use Nix         | ✅                            |

## nix-sandboxes

For several years now, i used to practice Katas in a Dojo, and i hate waiting to have an installation that is only _half_ working and absolutly not reproductible ; so i created a few [starters to have a simple setup for many languages](https://gitlab.com/pinage404/nix-sandboxes)

## Nix anywhere

Now, when i start working on [any project](https://git-gamble.is-cool.dev), i first add the configuration for Nix Flake and DirEnv

## `nixpkgs`

Since 2024, i have begun to contribute to [upstream packaging](https://github.com/NixOS/nixpkgs/pulls?q=is%3Apr+author%3Apinage404) which is no longer repackaged in this repository
