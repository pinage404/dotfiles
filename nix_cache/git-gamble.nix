{
  nix.settings = {
    substituters = [ "https://git-gamble.cachix.org" ];
    trusted-substituters = [ "https://git-gamble.cachix.org" ];
    trusted-public-keys = [ "git-gamble.cachix.org-1:afbVJAcYMKSs3//uXw3HFdyKLV66/KvI4sjehkdMM/I=" ];
  };
}
