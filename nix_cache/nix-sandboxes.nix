{
  nix.settings = {
    substituters = [ "https://pinage404-nix-sandboxes.cachix.org" ];
    trusted-substituters = [ "https://pinage404-nix-sandboxes.cachix.org" ];
    trusted-public-keys = [
      "pinage404-nix-sandboxes.cachix.org-1:5zGRK2Ou+C27E7AdlYo/s4pow/w39afir+KRz9iWsZA="
    ];
  };
}
