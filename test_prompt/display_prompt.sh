#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail -o errtrace

# shellcheck disable=SC2046
test_prompt_folder="$(realpath "$(dirname "$0")")"
cd "$test_prompt_folder"

DIRECTORIES=$(find "$test_prompt_folder" -maxdepth 1 -type d)
for directory in $DIRECTORIES; do
	cd "$directory"
	direnv allow
	direnv exec . starship prompt
	cd "$test_prompt_folder"
done

do_and_prompt() {
	cmd="$*"
	starship prompt
	echo "$cmd"
	eval "$cmd"
}

temp_dir=$(mktemp --directory)

cd "$temp_dir"

do_and_prompt git init
do_and_prompt git commit --allow-empty --message 'initial-commit'
do_and_prompt touch some_file.txt
do_and_prompt git add some_file.txt
do_and_prompt git commit --allow-empty-message --no-edit
do_and_prompt git mv some_file.txt renamed_file.txt
do_and_prompt mv renamed_file.txt some_file.txt
do_and_prompt git stash
do_and_prompt git stash drop
do_and_prompt rm some_file.txt
do_and_prompt git rm some_file.txt

cd "$test_prompt_folder"
