let
  framework-16 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINLi9TmOzUK/YRyp7thGu+t1YkLxdzIYgt/97UOnHuUL root@nixos";
  dell-optiplex-3050 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFVe7+wptbdPvLwUtMHfSYs++00cqZhXD0tnImc66rNr root@nixos";
in
{
  "gitlab-runner/framework-16/default.age".publicKeys = [ framework-16 ];
  "gitlab-runner/framework-16/default-2.age".publicKeys = [ framework-16 ];
  "gitlab-runner/framework-16/default-3.age".publicKeys = [ framework-16 ];
  "gitlab-runner/framework-16/nix.age".publicKeys = [ framework-16 ];
  "gitlab-runner/dell-optiplex-3050/default.age".publicKeys = [
    framework-16
    dell-optiplex-3050
  ];
  "gitlab-runner/dell-optiplex-3050/default-2.age".publicKeys = [
    framework-16
    dell-optiplex-3050
  ];
  "gitlab-runner/dell-optiplex-3050/default-3.age".publicKeys = [
    framework-16
    dell-optiplex-3050
  ];
  "gitlab-runner/dell-optiplex-3050/nix.age".publicKeys = [
    framework-16
    dell-optiplex-3050
  ];
  "spotifyd.age".publicKeys = [
    framework-16
    dell-optiplex-3050
  ];
  "nix/github/personnal_access_token.age".publicKeys = [ framework-16 ];
}
