{
  config,
  lib,
  pkgs,
  ...
}:

let

  cfg = config.services.automatic-shutdown;

in
{
  options = {
    services.automatic-shutdown = {
      enable = lib.mkEnableOption ''
        Shutdown the machine at the given time
      '';

      time = lib.mkOption {
        type = lib.types.str;
        example = "00:30";
        description = ''
          When to shutdown the machine.
          The format is described in
          <citerefentry>
            <refentrytitle>shutdown</refentrytitle>
            <manvolnum>8</manvolnum>
          </citerefentry>.
        '';
      };
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.user.services.automatic-shutdown = {
      Unit = {
        Description = "Automatic shutdown";
      };

      Install = {
        WantedBy = [ "default.target" ];
      };

      Service = {
        ExecStart = "${lib.getExe' pkgs.systemd "shutdown"} ${cfg.time}";
      };
    };
  };
}
