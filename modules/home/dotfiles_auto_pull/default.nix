{
  config,
  lib,
  pkgs,
  inputs,
  ...
}:

let
  home-manager-lib = inputs.home-manager.lib;
  cfg = config.services.dotfiles_auto_pull;
in
{
  options = {
    services.dotfiles_auto_pull = {
      enable = lib.mkEnableOption "dotfiles pull";

      frequency = lib.mkOption {
        type = lib.types.str;
        default = "12:00";
        example = "weekly";
        description = ''
          How often or when pull is run.
          This value is passed to the systemd timer configuration as the onCalendar option.
          The format is described in
          <citerefentry>
            <refentrytitle>systemd.time</refentrytitle>
            <manvolnum>7</manvolnum>
          </citerefentry>.
        '';
      };

      dotfiles_dir = lib.mkOption {
        type = lib.types.path;
        default = config.home.homeDirectory + "/Project/dotfiles";
        example = "config.home.homeDirectory + \"/dotfiles\"";
        description = "Absolute path where are stored your dotfiles.";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    assertions = [
      (home-manager-lib.hm.assertions.assertPlatform "services.dotfiles_auto_pull" pkgs
        lib.platforms.linux
      )
    ];

    systemd.user = {
      timers.dotfiles_auto_pull = {
        Unit = {
          Description = "dotfiles pull timer";
        };

        Install = {
          WantedBy = [ "timers.target" ];
        };

        Timer = {
          OnCalendar = cfg.frequency;
          Unit = "dotfiles_auto_pull.service";
        };
      };

      services.dotfiles_auto_pull = {
        Unit = {
          Description = "dotfiles pull";
          After = [ "network-online.target" ];
          Wants = [ "network-online.target" ];
        };

        Service = {
          ExecStart = ''
            ${lib.getExe pkgs.gitMinimal} \
              -C ${cfg.dotfiles_dir} \
              pull \
              --ff-only \
              --autostash
          '';
        };
      };
    };
  };
}
